package de.jolly.eventwarp.players;

class LastDamager {

	/**
	 * Der maxDamageDelay gibt an, wie viele Millisekunden maximal seit dem letzten Schaden vergangen sein d�rfen, dass der letzte Damager noch als Verursacher eines m�glichen Todes gilt.
	 */
	static final int maxDamageDelay = 5000;

	/**
	 * Der Spieler, der zuletzt Schaden verursacht hat oder null, wenn es keinen solchen gibt.
	 */
	private Damager lastDamager;
	/**
	 * Der Zeitpunkt, ermittelt mit System.currentTimeMillis(), an dem zuletzt Schaden genommen wurde. 
	 */
	private long lastDamageTime;

	LastDamager() {
		lastDamageTime = 0;
	}

	/**
	 * Speichert, dass soeben von dem �bergebenen EventPlayer Schaden eingegangen ist.
	 * @param damager
	 */
	public void DamageTaken(Damager damager) {
		lastDamager = damager;
		lastDamageTime = System.currentTimeMillis();
	}

	/**
	 * Gibt den Spieler zur�ck, der zuletzt Schaden verursacht hat.
	 * Gibt es keinen solchen oder ist der letzte Schaden zu lange her, wird null zur�ckgegeben
	 * @return Den Spieler, der zuletzt Schaden verursacht hat oder null, wenn das Zeitlimit abgelaufen ist oder noch kein Schaden verursacht wurde
	 */
	public Damager getLastDamager() {
		if (System.currentTimeMillis() - lastDamageTime > maxDamageDelay) {
			return null;
		} else {
			return lastDamager;
		}
	}
}
