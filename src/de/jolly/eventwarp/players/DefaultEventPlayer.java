package de.jolly.eventwarp.players;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.OverridingMethodsMustInvokeSuper;

import de.jolly.eventwarp.controls.SpawnLocationManager;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import de.jolly.eventwarp.event.EventwarpPlayerChangeLivesEvent;
import de.jolly.eventwarp.event.EventwarpPlayerEnterSpectatormodeEvent;
import de.jolly.eventwarp.event.EventwarpPlayerLeaveSpectatormodeEvent;
import de.jolly.eventwarp.event.EventwarpPlayerSwitchTeamEvent;
import de.jolly.eventwarp.settings.tangible.PlayerLivesSetting;

public class DefaultEventPlayer implements EventPlayer {

	/**
	 * The Player this EventPlayer refers to
	 */
	private Player p;
	/**
	 * The PlayerManager who is taking care of this EventPlayer
	 */
	private PlayerManager manager;
	/**
	 * The LastDamager-instance which storages who was the last one to damage this player and when it was.
	 */
	private LastDamager lastDamager;

	/**
	 * The amount of lives this EventPlayer has
	 */
	private int lives;

	/**
	 * If this EventPlayer is a spectator
	 */
	private boolean spectator;

	/**
	 * This player's spawn group
	 */
	private String spawngroup;

	/**
	 * This player's team
	 */
	private Team team;

	/**
	 * Creates the EventPlayer.
	 *
	 * @param p       The Player instance this EventPlayer shall refer to.
	 * @param manager The PlayerManager which is taking care of this EventPlayer
	 */
	protected DefaultEventPlayer(Player p, PlayerManager manager) {
		this.p = p;
		this.manager = manager;
		lastDamager = new LastDamager();
		lives = 0;
		p.setLevel(0);
		p.setExp(0.0f);
		spectator = false;
		spawngroup = de.jolly.eventwarp.controls.SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP;
		team = null;
	}

	@Override
	public Player getPlayer() {
		return p;
	}

	@Override
	public String getName() {
		return p.getName();
	}

	@Override
	public Damager getLastDamager() {
		return lastDamager.getLastDamager();
	}

	@Override
	public void registerDamage(Damager d) {
		lastDamager.DamageTaken(d);
	}

	@Override
	@OverridingMethodsMustInvokeSuper
	public void kick() {
		manager.kickPlayer(this);
	}

	@Override
	@OverridingMethodsMustInvokeSuper
	public void ban() {
		manager.kickPlayer(this);
		manager.getMain().getBanlist().banPlayer(p);
	}

	@Override
	public void sendMessage(String message) {
		manager.getMain().sendMessage(p, message);
	}

	@Override
	@OverridingMethodsMustInvokeSuper
	public void respawn(boolean teleport) {
		if (isSpectatormode()) {
			p.getInventory().clear();
		} else {
			p.getInventory().setContents(manager.getWarp().getEventEquipment().getKit());
			p.getInventory().setArmorContents(manager.getWarp().getEventEquipment().getArmor());
		}
		p.setLevel(lives);

		if (teleport) {
			Location spawn = manager.getRespawnLocation(this);
			if (spawn != null) {
				p.teleport(spawn);
			}
		}
		manager.notifyRespawn(this);
	}


	@Override
	public void reset() {
		setLives(((PlayerLivesSetting) manager.getWarp().getSettings().getSetting("lives")).getValue());
		if (isSpectatormode()) {
			disableSpectatormode();
			//Which includes respawning
		} else {
			respawn(true);
		}
	}

	@Override
	public String toString() {
		return getName();
	}

	@Override
	public int getLives() {
		return lives;
	}

	@Override
	public void setLives(int lives) {
		lives = Math.max(lives, 0);
		if (this.lives == lives) {
			return;
		}
		getPlayer().setLevel(lives);
		EventwarpPlayerChangeLivesEvent event = new EventwarpPlayerChangeLivesEvent(this, lives - this.lives);
		if (manager.getWarp().callEvent(event)) {
			this.lives = lives;
		}
		manager.getGUI().update();
	}

	@Override
	@OverridingMethodsMustInvokeSuper
	public void enableSpectatormode() {
		if (spectator) return;
		spectator = true;
		manager.getSpectatorManager().enableSpectatormode(this);
		respawn(true);
		EventwarpPlayerEnterSpectatormodeEvent event = new EventwarpPlayerEnterSpectatormodeEvent(this);
		manager.getWarp().callEvent(event);
		manager.getGUI().update();
		manager.getMain().sendMessage(this.getPlayer(), "�9Du bist nun Zuschauer.");
	}

	@Override
	@OverridingMethodsMustInvokeSuper
	public void disableSpectatormode() {
		if (!spectator) return;
		spectator = false;
		manager.getSpectatorManager().disableSpectatormode(this);
		respawn(true);
		EventwarpPlayerLeaveSpectatormodeEvent event = new EventwarpPlayerLeaveSpectatormodeEvent(this);
		manager.getWarp().callEvent(event);
		manager.getGUI().update();
		manager.getMain().sendMessage(this.getPlayer(), "�9Du bist kein Zuschauer mehr.");
	}

	@Override
	public boolean isSpectatormode() {
		return spectator;
	}

	@Override
	public List<String> getStatus() {
		return getStatus(0);
	}

	/**
	 * This internal method is used to modify the size of the List to improve the performance. Use this method when
	 * overriding getStatus() and appending your own status information to the List and tell this method how many
	 * additional entries you'll create to keep the performance high.
	 *
	 * @param additionalsize The additional amount of entries to be reserved in the List, used to avoid having to
	 *                       recreate the internal array when adding additional entries.
	 * @return A List containing Strings which holds all relevant information on this EventPlayer-instance. This List
	 * has the in the parameter stated given amount of extra space for additional entries.
	 */
	protected final List<String> getStatus(int additionalsize) {
		List<String> lore = new ArrayList<>(additionalsize + 3);
		lore.add("�9Leben: �a" + getLives());
		if (isSpectatormode()) {
			lore.add("�9Spectatormode: �aaktiv");
		} else {
			lore.add("�9Spectatormode: �cinaktiv");
		}
		if (team == null) {
			lore.add("�9Team: �8keines");
		} else {
			lore.add("�9Team: " + team.getChatName());
		}
		lore.add("�9Spawngruppe: �d" + spawngroup);
		return lore;
	}

	@Override
	public void setClothesColor(Color color) {
		ItemStack[] armor = getPlayer().getInventory().getArmorContents();
		for (int i = 0; i < armor.length; i++) {
			if (armor[i] != null) {
				if (armor[i].getType() == Material.LEATHER_BOOTS ||
						armor[i].getType() == Material.LEATHER_LEGGINGS ||
						armor[i].getType() == Material.LEATHER_CHESTPLATE ||
						armor[i].getType() == Material.LEATHER_HELMET) {
					LeatherArmorMeta meta = (LeatherArmorMeta) armor[i].getItemMeta();
					meta.setColor(color);
					armor[i].setItemMeta(meta);
				}
			}
		}
	}

	@Override
	public String getSpawngroup() {
		return spawngroup;
	}

	@Override
	public void setSpawngroup(String spawngroup) {
		if (spawngroup == null) return;
		this.spawngroup = spawngroup;
	}

	@Override
	public Team getTeam() {
		return team;
	}

	@Override
	public void setTeam(Team team) {
		if (this.team == team) return;
		Team old = this.team;
		this.team = team;
		setSpawngroup(team == null ? SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP : team.getDefaultSpawngroup());
		manager.getWarp().callEvent(new EventwarpPlayerSwitchTeamEvent(this, old));
	}

}
