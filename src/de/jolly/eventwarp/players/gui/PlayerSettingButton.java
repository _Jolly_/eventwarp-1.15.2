package de.jolly.eventwarp.players.gui;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.players.EventPlayer;

public class PlayerSettingButton extends ItemButton {
	
	EventPlayer player;
	private final String permission;
	private final int slot;
	private PlayerButtonTask task;
	
	/**
	 * Warning! The attribute "player" is most likely
	 * needed within the class and hence needs to be instantiated!
	 */
	public PlayerSettingButton(Material material, String title, String permission, int slot) {
		super(material, title, null);
		this.permission = permission;
		this.slot = slot;
		super.setTask(internalTask);
	}
	
	private final ButtonTask internalTask = new ButtonTask() {

		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			Player p = (Player) e.getWhoClicked();
			if (p.hasPermission(permission)) {
				if (task != null) {
					task.execute(PlayerSettingButton.this, e);
				}
				p.playSound(p.getLocation(), Sound.BLOCK_WOODEN_BUTTON_CLICK_ON, 1, 1);
			} else {
				p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
				p.sendMessage("�cDu ben�tigst die Permission �defS"+permission+"�cf�r diese Aktion.");
			}
		}
		
	};
	
	public void setTask(PlayerButtonTask task) {
		this.task = task;
	}
	
	public int getSlot() {
		return slot;
	}
	
	public EventPlayer getPlayer() {
		return player;
	}
	
	/**
	 * Sets the Inventory used by this button
	 */
	public void setInventory(InteractionInventory inv) {
		this.inv = inv;
	}
	
	@Override
	public PlayerSettingButton clone() {
		PlayerSettingButton button = new PlayerSettingButton(getType(), getItemMeta().getDisplayName(), permission, slot);
		button.setDurability(getDurability());
		button.setAmount(getAmount());
		button.setItemMeta(getItemMeta().clone());
		button.player = this.player;
		button.setTask(task);
		return button;
	}
}
