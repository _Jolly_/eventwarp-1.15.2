package de.jolly.eventwarp.players.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.SkullMeta;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;

/**
 * This is the Button which appears in the PlayerManager GUI for every player,
 * both in the list and in the detailed view, however with an empty task at the detailed view.
 * @author _Jolly_
 *
 */
public class PlayerButton extends ItemButton {
	
	private final EventPlayer p;
	private final VisualPlayerList list;
	
	public PlayerButton(EventPlayer p, VisualPlayerList list, InteractionInventory inv, Eventwarp warp) {
		super(Material.PLAYER_HEAD, inv);
		
		this.p = p;
		this.list = list;

		SkullMeta meta = (SkullMeta) getItemMeta();
		meta.setDisplayName("�b"+p.getName());
		meta.setOwningPlayer(p.getPlayer());
		meta.setLore(p.getStatus());
		setItemMeta(meta);
		
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			list.loadPlayerPage((Player)e.getWhoClicked(), p);
		}
	};
}
