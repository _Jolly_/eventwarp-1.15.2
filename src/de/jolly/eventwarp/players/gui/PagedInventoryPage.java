package de.jolly.eventwarp.players.gui;

import org.bukkit.entity.Player;

import codetools.inventory.InteractionInventory;

public class PagedInventoryPage extends InteractionInventory {

	private final PagedInventory main;
	private final int page;
	
	protected PagedInventoryPage(PagedInventory main, int page) {
		super(main.size, main.title);
		this.main = main;
		this.page = page;
	}
	
	/**
	 * Note: This only works when the player already has a suitable Inventory open!
	 * Also, this method does not register the player in the list in the class PagedInventory, which storages
	 * which player is currently viewing which page.
	 * To sum it up, it is unsafe to directly use this method.
	 */
	@Override
	public void showToPlayer(Player p) {
		currentUsers.add(p);
		update();
	}
	/**
	 * Unregisters this player as a current Viewer.
	 * Using this method is unsafe, as the Player is not removed
	 * from the list in the class PagedInventory, which storages
	 * which player is currently viewing which page.
	 * Use onInvClosed() for safely leaving the page.
	 */
	public void removeViewer(Player p) {
		currentUsers.remove(p);
	}
	
	/**
	 * This method unregisters the Player
	 * and unloads this page if no other player is
	 * currently viewing this page.
	 */
	@Override
	public void onInvClosed(Player p) {
		super.onInvClosed(p);
		currentUsers.remove(p);
		main.onViewLeft(p, this);
	}
	
	boolean hasViewers() {
		return currentUsers.size() > 0;
	}
	int getPageNumber() {
		return page;
	}
}
