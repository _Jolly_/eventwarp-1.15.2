package de.jolly.eventwarp.players.gui;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import codetools.inventory.ButtonTask;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;

class DetailedPlayerPage {
	
	private final VisualPlayerList list;
	private final Eventwarp warp;
	
	private final Set<PlayerSettingButton> buttons;
	
	public DetailedPlayerPage(final VisualPlayerList list, final Eventwarp warp) {
		this.list = list;
		this.warp = warp;
		buttons = new HashSet<>();
	}
	
	void insertContents(final DetailedPlayerPageInventory inv) {
		
		ItemStack[] contents = new ItemStack[54];
		final EventPlayer p = inv.getPlayer();
		ItemButton playerHead = new PlayerButton(p, list, inv, warp);
		playerHead.setTask(null);
		
		contents[4] = playerHead.clone();
		
		for (PlayerSettingButton b: buttons) {
			PlayerSettingButton newbutton = b.clone();
			newbutton.player = p;
			newbutton.setInventory(inv);
			contents[b.getSlot()] = newbutton;
		}
		
		contents[45] = new ItemButton(Material.BLUE_WOOL, "�bZur�ck", new ButtonTask() {
			@Override
			public void execute(ItemButton button, InventoryClickEvent e) {
				list.leavePlayerPage((Player)e.getWhoClicked(), inv);
			}
		}, inv);
		
		for (int i = 46; i < 54; i++) {
			contents[i] = new ItemButton(Material.BEDROCK, inv);
		}
		
		inv.setContents(contents);
	}
	
	public void registerPlayerSettingButton(PlayerSettingButton b) {
		buttons.add(b);
	}
	public void unregisterPlayerSettingButton(PlayerSettingButton b) {
		buttons.remove(b);
	}
	public void unregisterAllPlayerSettingButtons() {
		buttons.clear();
	}
	
}
