package de.jolly.eventwarp.players.gui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.players.gui.buttons.BanButton;
import de.jolly.eventwarp.players.gui.buttons.KickButton;
import de.jolly.eventwarp.players.gui.buttons.KillButton;
import de.jolly.eventwarp.players.gui.buttons.ResetButton;
import de.jolly.eventwarp.players.gui.buttons.RespawnButton;
import de.jolly.eventwarp.players.gui.buttons.SetLivesButton;
import de.jolly.eventwarp.players.gui.buttons.ToggleSpectatormodeButton;
import de.jolly.eventwarp.settings.ControlPanel;

public class VisualPlayerList extends PagedInventory implements Listener, EventwarpListener {

	static final int SIZE = 54;

	private final EventwarpMain main;

	private final PlayerListPage listPageInstance;
	private final DetailedPlayerPage playerPageInstance;

	/**
	 * This Map shows the Players which are currently in
	 * the detailed view of a specific Player.
	 */
	private final Map<Player, EventPlayer> playerView;
	private final Map<EventPlayer, DetailedPlayerPageInventory> plLoaded;

	public VisualPlayerList(Eventwarp warp, PlayerManager manager, ControlPanel panel, EventwarpMain main) {
		super(SIZE, "�3Eventwarp - Spieler");
		playerView = new HashMap<>();
		plLoaded = new HashMap<>();
		this.main = main;
		listPageInstance = new PlayerListPage(warp, manager, this, panel, main);
		playerPageInstance = new DetailedPlayerPage(this, warp);
	}

	/**
	 * Fully reloads the VisualPlayerList.
	 * This resets the PlayerSettingButtons
	 */
	public void reload() {
		playerPageInstance.unregisterAllPlayerSettingButtons();
		registerDefaultPlayerSettingButtons();
		update();
	}

	@Override
	protected void insertContents(PagedInventoryPage p) {
		listPageInstance.insertContents(p);
	}
	protected void insertPlayerContents(DetailedPlayerPageInventory inv) {
		playerPageInstance.insertContents(inv);
	}

	protected DetailedPlayerPageInventory getPlayerPageInv(EventPlayer player) {
		DetailedPlayerPageInventory p = plLoaded.get(player);
		if (p == null) {
			p = new DetailedPlayerPageInventory(this, player);
			insertPlayerContents(p);
			plLoaded.put(player, p);
		}
		return p;
	}

	/**
	 * Makes the Player switch to the detailed view of a Player
	 * @param p The Player who switches to the detailed view
	 * @param target Whoever the detailed view is about
	 */
	void loadPlayerPage(Player p, EventPlayer target) {
		/* Here I pretend that the player did not leave the list view, so
		 * it is still saved at which page the player was and the page will (in most cases)
		 * not be unloaded, but he is unregistered as viewer to avoid problems with the
		 * InteractionInventoryListener, as it would seem like the player accesses two
		 * InteractionInventories simultaneously
		 */
		getPageInstance(currentpage.get(p)).removeViewer(p);

		playerView.put(p, target);
		DetailedPlayerPageInventory inv = getPlayerPageInv(target);
		inv.showToPlayer(p);
	}
	/**
	 * Makes this Player leave the detailed view.
	 */
	void leavePlayerPage(Player p, DetailedPlayerPageInventory inv) {
		inv.removeViewer(p);
		onLeaveDetailedView(p, inv);
		
		int page = currentpage.get(p);
		getPageInstance(page).showToPlayer(p);
	}
	private void onLeaveDetailedView(Player p, DetailedPlayerPageInventory inv) {
		playerView.remove(p);
		if (!inv.hasViewers()) {
			plLoaded.remove(inv.getPlayer());
			inv.dispose();
		}
	}
	
	
	@Override
	protected void onViewLeft(Player p, PagedInventoryPage inv) {
		if (inv instanceof DetailedPlayerPageInventory) {
			onLeaveDetailedView(p, (DetailedPlayerPageInventory) inv);
			
			PagedInventoryPage old =  getPageInstance(currentpage.get(p));
			old.onInvClosed(p);
		} else {
			super.onViewLeft(p, inv);
		}
	}

	@Override
	public void update() {
		long time = System.currentTimeMillis();
		main.debug.send("PlayerManager GUI wird aktualisiert.");
		VisualPlayerList.super.update();
		for (DetailedPlayerPageInventory inv: plLoaded.values()) {
			insertPlayerContents(inv);
			inv.update();
		}
		main.debug.send("Aktualisierung abgeschlossen. Dauer: �a"+(System.currentTimeMillis()-time)+"ms");
	}

	private void registerDefaultPlayerSettingButtons() {
		playerPageInstance.registerPlayerSettingButton(new RespawnButton(main));
		playerPageInstance.registerPlayerSettingButton(new KillButton(main));
		playerPageInstance.registerPlayerSettingButton(new KickButton(main));
		playerPageInstance.registerPlayerSettingButton(new BanButton(main));
		playerPageInstance.registerPlayerSettingButton(new ToggleSpectatormodeButton(main));
		playerPageInstance.registerPlayerSettingButton(new SetLivesButton(main));
		playerPageInstance.registerPlayerSettingButton(new ResetButton(main));
	}
	
	@Override
	public void onJoin(EventPlayer player) {
		update();
	}

	@Override
	public void onLeave(EventPlayer player) {
		if (playerView.containsValue(player)) {
			for (Player all: playerView.keySet()) {
				if (playerView.get(all) == player) {
					leavePlayerPage(all, plLoaded.get(player));
				}
			}
		}

		update();
	}

	@Override
	public void onRespawn(EventPlayer player) {}

	@Override
	public void onDeath(EventPlayer player) {}
	
}
