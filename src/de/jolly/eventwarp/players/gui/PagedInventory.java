package de.jolly.eventwarp.players.gui;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public abstract class PagedInventory {
	
	/**
	 * The page the given Players are currently on.
	 * The first page has the index 0
	 */
	protected final Map<Player, Integer> currentpage;
	
	private final Map<Integer, PagedInventoryPage> loaded;
	
	final String title;
	final int size;
	
	public PagedInventory(int size, String title) {
		currentpage = new HashMap<>();
		loaded = new HashMap<>();
		this.title = title;
		this.size = size;
	}
	public PagedInventory(int size) {
		currentpage = new HashMap<>();
		loaded = new HashMap<>();
		this.size = size;
		title = "";
	}
	
	/**
	 * This method is called whenever a player
	 * switches to a new View or closed the Inventory
	 * @param inv The View that was left
	 */
	protected void onViewLeft(Player p, PagedInventoryPage inv) {
		currentpage.remove(p);
		if (!inv.hasViewers()) {
			loaded.remove(inv.getPageNumber());
			inv.dispose();
		}
	}
	
	public void openInventory(Player p) {
		Inventory inv = Bukkit.createInventory(null, size, title);
		p.openInventory(inv);
		showPage(p, 0);
	}
	
	/**
	 * Lets the given player show the given page in this
	 * PagedInventory
	 */
	public void showPage(Player p, int newpage) {
		Integer old = currentpage.get(p);
		if (old != null) {
			loaded.get(old).removeViewer(p);
			onViewLeft(p, loaded.get(old));
		}
		
		currentpage.put(p, newpage);
		getPageInstance(newpage).showToPlayer(p);
	}
	
	protected PagedInventoryPage getPageInstance(int page) {
		PagedInventoryPage p = loaded.get(page);
		if (p == null) {
			p = new PagedInventoryPage(this, page);
			insertContents(p);
			loaded.put(page, p);
		}
		return p;
	}
	
	/**
	 * Inserts the Contents into that PagedInventoryPage.
	 * The page holds the information which page in the list it
	 * represents (getPageNumber())
	 */
	protected abstract void insertContents(PagedInventoryPage p);
	
	public void update() {
		for (PagedInventoryPage p: loaded.values()) {
			insertContents(p);
			p.update();
		}
	}
	
}
