package de.jolly.eventwarp.players.gui;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;

class SwitchPageButton extends ItemButton {
	
	private final int pagemovement;
	private final int currentpage;
	private final boolean blocked;
	
	private final PagedInventory inv;
	
	/**
	 * Creates the SwitchPageButton
	 * @param pagemovement This parameter indicates how many pages this button
	 * will bring you. If the value is negative, the button will move you backwards in the list
	 * of pages.
	 * @param currentpage The number of the page this button is placed on.
	 * @param blocked If true, the button will be set to disabled state, effectively stopping players
	 * from being able to use it. The button might set itself to blocked state if for example the
	 * pagemovement is -1 and the current page is 0, as you would move to a not existing page.
	 */
	SwitchPageButton(final int pagemovement, final int currentpage, final boolean blocked, final String title, final PagedInventory inv, InteractionInventory i) {
		super(Material.WHITE_WOOL, title, i);
		
		this.inv = inv;
		
		this.pagemovement = pagemovement;
		
		if (currentpage < 0) {
			this.currentpage = 0;
		} else {
			this.currentpage = currentpage;
		}
		
		if (blocked) {
			this.blocked = true;
		} else {
			if (currentpage+pagemovement < 0) {
				this.blocked = true;
			} else {
				this.blocked = false;
			}
		}
		
		if (this.blocked) {
			setType(Material.RED_WOOL);
		} else {
			setType(Material.CYAN_WOOL);
		}
		
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			Player p = (Player) e.getWhoClicked();
			if (blocked) {
				p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
			} else {
				p.playSound(p.getLocation(), Sound.BLOCK_WOODEN_BUTTON_CLICK_ON, 1, 1);
				inv.showPage(p, currentpage+pagemovement);
			}
		}
	};
}
