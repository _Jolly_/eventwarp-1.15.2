package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class RespawnButton extends PlayerSettingButton {
	
	private final EventwarpMain main;
	
	public RespawnButton(EventwarpMain main) {
		super(Material.COMPASS, "žaSpieler respawnen", main.permission()+".respawn", 19);
		this.main = main;
		setTask(task);
	}
	
	private final PlayerButtonTask task = new PlayerButtonTask() {

		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			button.getPlayer().respawn(true);
			main.sendMessage(button.getPlayer().getPlayer(), "Du wurdest zum Respawn gezwungen.");
		}
		
	};
}
