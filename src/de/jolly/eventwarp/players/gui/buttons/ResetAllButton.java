package de.jolly.eventwarp.players.gui.buttons;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.PlayerManager;

public class ResetAllButton extends ItemButton {
	
	private final PlayerManager manager;
	private final EventwarpMain main;
	
	public ResetAllButton(PlayerManager manager, EventwarpMain main, InteractionInventory inv) {
		super(Material.EXPERIENCE_BOTTLE, "�aAlle Spieler resetten", inv);
		ItemMeta meta = getItemMeta();
		List<String> lore = new ArrayList<>(2);
		lore.add("Damit respawnst du alle Spieler,");
		lore.add("f�llst alle Leben auf");
		lore.add("und deaktiviert den Spectatormode");
		lore.add("bei allen Spielern.");
		meta.setLore(lore);
		setItemMeta(meta);
		this.main = main;
		this.manager = manager;
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {

		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			String permission = main.permission()+".reset";
			Player p = (Player) e.getWhoClicked();
			if (p.hasPermission(permission)) {
				manager.resetAllPlayers();
				manager.broadcastMessage("Alle Leben wurden aufgef�llt.");
				p.playSound(p.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
			} else {
				p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
				p.sendMessage("�cDu ben�tigst die Permission �defS"+permission+"�cf�r diese Aktion.");
			}
		}
		
	};
	
}
