package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;

public class KillAllButton extends ItemButton {

	private final PlayerManager manager;
	private final EventwarpMain main;
	
	public KillAllButton(PlayerManager manager, EventwarpMain main, InteractionInventory inv) {
		super(Material.DIAMOND_SWORD, "�aAlle Spieler t�ten", inv);
		this.main = main;
		this.manager = manager;
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {

		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			String permission = main.permission()+".kill";
			Player p = (Player) e.getWhoClicked();
			if (p.hasPermission(permission)) {
				for (EventPlayer player: manager.getAllPlayers()) {
					player.getPlayer().setHealth(0);
				}
				manager.broadcastMessage("Alle Spieler wurden get�tet!");
				p.playSound(p.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
			} else {
				p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
				p.sendMessage("�cDu ben�tigst die Permission �defS"+permission+"�cf�r diese Aktion.");
			}
		}
		
	};
	
}
