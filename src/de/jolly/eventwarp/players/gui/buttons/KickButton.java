package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class KickButton extends PlayerSettingButton {

	private final EventwarpMain main;

	public KickButton(EventwarpMain main) {
		super(Material.REDSTONE_BLOCK, "�cSpieler kicken", main.permission()+".kick", 21);
		this.main = main;
		setTask(task);
	}

	private final PlayerButtonTask task = new PlayerButtonTask() {

		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			Player p = button.getPlayer().getPlayer();
			button.getPlayer().kick();
			main.sendMessage(p, "Du wurdest vom Event gekickt.");
		}

	};

}
