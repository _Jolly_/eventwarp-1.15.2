package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.PlayerManager;

public class KickAllButton extends ItemButton {

	private final PlayerManager manager;
	private final EventwarpMain main;
	
	public KickAllButton(PlayerManager manager, EventwarpMain main, InteractionInventory inv) {
		super(Material.IRON_BOOTS, "�aAlle Spieler kicken", inv);
		this.main = main;
		this.manager = manager;
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {

		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			String permission = main.permission()+".kick";
			Player p = (Player) e.getWhoClicked();
			if (p.hasPermission(permission)) {
				manager.broadcastMessage("Alle Spieler werden gekickt!");
				manager.kickAllPlayers();
				p.playSound(p.getLocation(), Sound.BLOCK_STONE_BUTTON_CLICK_ON, 1, 1);
			} else {
				p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
				p.sendMessage("�cDu ben�tigst die Permission �defS"+permission+"�cf�r diese Aktion.");
			}
		}
		
	};
	
}
