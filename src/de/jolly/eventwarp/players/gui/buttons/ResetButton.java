package de.jolly.eventwarp.players.gui.buttons;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class ResetButton extends PlayerSettingButton {
	
	private final EventwarpMain main;
	
	public ResetButton(EventwarpMain main) {
		super(Material.EXPERIENCE_BOTTLE, "�aSpieler resetten", main.permission()+".reset", 30);
		ItemMeta meta = getItemMeta();
		List<String> lore = new ArrayList<>(2);
		lore.add("Damit respawnst du den Spieler,");
		lore.add("f�llst seine Leben auf");
		lore.add("und deaktivierst den Spectatormode.");
		meta.setLore(lore);
		setItemMeta(meta);
		this.main = main;
		setTask(task);
	}
	
	private final PlayerButtonTask task = new PlayerButtonTask() {

		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			button.getPlayer().reset();
			main.sendMessage(button.getPlayer().getPlayer(), "Du wurdest zur�ckgesetzt. Deine Leben wurden aufgef�llt, der Spectatormode deaktiviert und du wurdest respawnt.");
		}
		
	};
	
}
