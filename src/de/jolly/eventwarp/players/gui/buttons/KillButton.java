package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class KillButton extends PlayerSettingButton {
	
	private final EventwarpMain main;
	
	public KillButton(EventwarpMain main) {
		super(Material.DIAMOND_SWORD, "�eSpieler t�ten", main.permission()+".kill", 20);
		this.main = main;
		setTask(task);
	}
	
	private final PlayerButtonTask task = new PlayerButtonTask() {

		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			button.getPlayer().getPlayer().setHealth(0);
			main.sendMessage(button.getPlayer().getPlayer(), "Du wurdest von einem Admin umgebracht.");
		}
		
	};
	
}
