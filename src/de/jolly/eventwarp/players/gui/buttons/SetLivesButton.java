package de.jolly.eventwarp.players.gui.buttons;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class SetLivesButton extends PlayerSettingButton {
	
	public SetLivesButton(EventwarpMain main) {
		super(Material.POTION, "�aLeben einstellen", main.permission()+".lives", 29);
		setDurability((short)8197);
		ItemMeta meta = getItemMeta();
		List<String> lore = new ArrayList<>();
		lore.add("Linksklick, um ein Leben hinzuzuf�gen.");
		lore.add("Rechtsklick, um ein Leben abzuziehen.");
		lore.add("Wenn du dem Spieler alle Leben entziehst,");
		lore.add("wird der Spectatormode trotzdem");
		lore.add("�c�onicht�5�o aktiviert.");
		meta.setLore(lore);
		setItemMeta(meta);
		
		setTask(task);
	}
	
	private final PlayerButtonTask task = new PlayerButtonTask() {
		
		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			EventPlayer p = button.getPlayer();
			if (e.isLeftClick()) {
				p.setLives(p.getLives()+1);
			} else {
				p.setLives(p.getLives()-1);
			}
		}
	};
}
