package de.jolly.eventwarp.players.gui.buttons;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.gui.PlayerButtonTask;
import de.jolly.eventwarp.players.gui.PlayerSettingButton;

public class ToggleSpectatormodeButton extends PlayerSettingButton {

	public ToggleSpectatormodeButton(EventwarpMain main) {
		super(Material.ENDER_EYE, "�9Spectatormode aktivieren/deaktivieren", main.permission()+".spectator", 28);
		setTask(task);
	}
	
	private final PlayerButtonTask task = new PlayerButtonTask() {
		
		@Override
		public void execute(PlayerSettingButton button, InventoryClickEvent e) {
			EventPlayer p = button.getPlayer();
			if (p.isSpectatormode()) {
				p.disableSpectatormode();
			} else {
				p.enableSpectatormode();
			}
		}
	};
	
}
