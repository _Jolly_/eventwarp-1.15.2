package de.jolly.eventwarp.players.gui.buttons;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

public class EnableSpectatorForAllButton extends ItemButton {

	public EnableSpectatorForAllButton(PlayerManager manager, InteractionInventory inv) {
		super(Material.ENDER_EYE, "�aAlle Spieler zu Zuschauern machen", inv);
		ButtonTask task = new ButtonTask() {

			@Override
			public void execute(ItemButton button, InventoryClickEvent e) {
				for (EventPlayer p : manager.getAllPlayers()) {
					p.enableSpectatormode();
				}
				manager.broadcastMessage("Alle Spieler wurden in den Spectatormode gesetzt.");
			}

		};
		setTask(task);
	}

}