package de.jolly.eventwarp.players.gui;

import de.jolly.eventwarp.players.gui.buttons.*;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.ControlPanel;
import de.jolly.eventwarp.settings.ReturnToPanelButton;

class PlayerListPage {
	
	private final Eventwarp warp;
	private final PlayerManager manager;
	private final VisualPlayerList inv;
	private final ControlPanel panel;
	private final EventwarpMain main;
	
	PlayerListPage(Eventwarp warp, PlayerManager manager, VisualPlayerList inv, ControlPanel panel, EventwarpMain main) {
		this.warp = warp;
		this.manager = manager;
		this.inv = inv;
		this.panel = panel;
		this.main = main;
	}
	
	/**
	 * Creates and insert the Contents for the given PagedInventoryPage
	 */
	void insertContents(PagedInventoryPage page) {
		EventPlayer[] all = manager.getAllPlayers();
		int size = VisualPlayerList.SIZE;
		int elementsPerPage = size-9;
		ItemStack[] contents = new ItemStack[size]; 
		int number = page.getPageNumber();
		
		if (all.length > number*elementsPerPage) {
			for (int i = 0; number*elementsPerPage+i < all.length && i < (number+1)*elementsPerPage; i++) {
				contents[i] = new PlayerButton(all[number*elementsPerPage+i], inv, page, warp);
			}
		}
		
		//Creating the Hotbar
		contents[size-9] = new ReturnToPanelButton(page, panel);
		
		contents[size-8] = new ItemButton(Material.BEDROCK, "�cDieser Block will eine Gehaltserh�hung.", page);
		
		contents[size-7] = new RespawnAllButton(manager, main, page);
		contents[size-6] = new ResetAllButton(manager, main, page);
		contents[size-5] = new KillAllButton(manager, main, page);
		contents[size-4] = new KickAllButton(manager, main, page);
		contents[size-3] = new EnableSpectatorForAllButton(manager, page);
			
		contents[size-2] = new SwitchPageButton(-1, number, false, "�dVorherige Seite", inv, page);
		contents[size-1] = new SwitchPageButton(1, number, !(all.length > ((number+1)*elementsPerPage)), "�dN�chste Seite", inv, page);
		
		page.setContents(contents);
	}

}
