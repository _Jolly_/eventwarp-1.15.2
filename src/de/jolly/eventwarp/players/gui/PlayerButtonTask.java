package de.jolly.eventwarp.players.gui;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface PlayerButtonTask {
	
	public void execute(PlayerSettingButton button, InventoryClickEvent e);
	
}
