package de.jolly.eventwarp.players.gui;

import de.jolly.eventwarp.players.EventPlayer;

public class DetailedPlayerPageInventory extends PagedInventoryPage {
	
	private final EventPlayer p;
	
	DetailedPlayerPageInventory(VisualPlayerList list, EventPlayer p) {
		super(list, 0);
		this.p = p;
	}
	
	EventPlayer getPlayer() {
		return p;
	}

}
