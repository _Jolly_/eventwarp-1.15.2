package de.jolly.eventwarp.players;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import codetools.main.CodeTools;
import codetools.removelist.RemoveList;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.controls.SpawnLocationManager;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.gui.VisualPlayerList;

/**
 * The PlayerManager takes care of a Set of EventPlayers
 * @author _Jolly_
 *
 */
public class PlayerManager implements Listener {

	
	/**
	 * The RemoveList to storage the data of the players.
	 */
	private final RemoveList removelist;
	/**
	 * The players this PlayerManager is taking care of
	 */
	private Set<EventPlayer> players;

	/**
	 * The main class of the Plugin
	 */
	private final EventwarpMain main;
	/**
	 * The Eventwarp this PlayerManager corresponds to
	 */
	private final Eventwarp warp;
	/**
	 * The GUI for this PlayerManager
	 */
	private final VisualPlayerList gui;
	/**
	 * The SpectatorManager
	 */
	private final SpectatorManager spectatorManager;
	
	/**
	 * This value determines the amount of active teams.
	 * If at least 1 team is active, new players will automatically be
	 * assigned to the teams.
	 * What teams are active is determined in the same order they appear in the Team enum.
	 */
	private int activeTeams;

	public PlayerManager(Eventwarp warp, EventwarpMain main) {
		this.warp = warp;
		this.main = main;
		players = new HashSet<>();
		removelist = new RemoveList(main);
		spectatorManager = new SpectatorManager(this, main);
		gui = new VisualPlayerList(warp, this, warp.getSettings().getControlPanel(), main);
		activeTeams = 0;
		main.getServer().getPluginManager().registerEvents(this, main);
		main.getServer().getPluginManager().registerEvents(spectatorManager, main);
	}

	/**
	 * Sends a message to all current Players at
	 * the Eventwarp.
	 * @param message The message to be broadcasted.
	 */
	public void broadcastMessage(String message) {
		for (EventPlayer all: getAllPlayers()) {
			warp.getAdapter().getMain().sendMessage(all.getPlayer(), message);
		}
		main.debug.send("Message broadcasted:\n"+message);
	}
	
	/**
	 * Creates a new EventPlayer-instance for the given player
	 * and makes this PlayerManager handle it.
	 * Has no effect if the PlayerManager already handles this Player
	 * @param p The Player to be added
	 */
	public void addPlayer(Player p) {
		if (getPlayer(p) != null) return;
		
		
		EventPlayer player = addPlayerToList(p);
		removelist.addToList(p);
		CodeTools.clearAllPotionEffects(p);
		assignPlayerToTeam(player);
		notifyJoin(player);
		player.reset();
	}
	/**
	 * Creates the EventPlayer-instance and adds the Player
	 * to the players-Set
	 * @return The created EventPlayer-instance
	 */
	private EventPlayer addPlayerToList(Player p) {
		EventPlayer player = warp.getAdapter().createEventPlayer(p, this);
		if (player == null) {
			player = new DefaultEventPlayer(p, this);
		}
		players.add(player);
		return player;
	}
	
	/**
	 * This method assigns the given player to the active team which currently has the fewest members.
	 * If no teams are set to active, this method simply quits.
	 */
	private void assignPlayerToTeam(EventPlayer p) {
		if (activeTeams < 1) {
			return;
		}
		Team[] teams = Team.values();
		int[] sizes = new int[teams.length];
		for (EventPlayer all: players) {
			Team current = all.getTeam();
			if (current == null) continue;
			for (int i = 0; i < teams.length; i++) {
				if (current == teams[i]) {
					sizes[i] = sizes[i]+1;
					break;
				}
			}
		}
		
		int smallestindex = 0;
		int smallestvalue = sizes[0];
		for (int i = 1; i < activeTeams; i++) {
			if (sizes[i] < smallestvalue) {
				smallestvalue = sizes[i];
				smallestindex = i;
			}
		}
		
		p.setTeam(teams[smallestindex]);
	}
	
	/**
	 * Removes this Player from the game. This resets him to the state he had
	 * before joining the Eventwarp.
	 * Has the same effect as using the kick()-Method on this EventPlayer
	 * @param p The EventPlayer to be removed.
	 */
	public void kickPlayer(EventPlayer p) {
		players.remove(p);
		p.getPlayer().getInventory().clear();
		removelist.useDataAndRemove(p.getPlayer());
		CodeTools.clearAllPotionEffects(p.getPlayer());
		notifyLeave(p);
	}
	/**
	 * Kicks all Players from the Eventwarp
	 */
	public void kickAllPlayers() {
		for (EventPlayer all: getAllPlayers()) {
			kickPlayer(all);
		}
	}

	/**
	 * Assigns all players to teams of equal numbers.
	 * @param teams The amount of teams to be filled.
	 * If this value is smaller than 0 or exceeds Team.values().length, nothing will happen.
	 * If this value is 0, all players will be kicked from their teams.
	 */
	public void assignTeams(int teams) {
		activeTeams = teams;
		getWarp().getSettings().getControlPanel().setActiveTeams(teams);

		Team[] team = Team.values();
		if (teams < 0 || team.length < teams) return;
		
		if (teams == 0) {
			
			for (EventPlayer all: players) {
				all.setTeam(null);
			}
			
		} else {
			
			int index = 0;
			for (EventPlayer all: players) {
				all.setTeam(team[index]);
				index++;
				if (index >= teams) {
					index = 0;
				}
			}
			
		}
		
		getWarp().getSettings().getControlPanel().update();
	}
	/**
	 * Clears all teams. This will set the amount of active teams to 0
	 * and sets every player's team to null.
	 */
	public void clearTeams() {
		assignTeams(0);
	}
	
	/**
	 * Forces all Players to respawn at the Eventwarp
	 */
	public void respawnAllPlayers() {
		for (EventPlayer all: players) {
			all.respawn(true);
		}
	}
	/**
	 * Refills lives of every player, disables Spectatormode for everyone
	 * and respawns every player.
	 */
	public void resetAllPlayers() {
		for (EventPlayer all: players) {
			all.reset();
		}
	}
	
	/**
	 * Saves the current data of the players to the RemoveList, overwriting
	 * the old data.
	 * DO NOT USE THIS DURING THE GAME as the old inventory
	 * of the players will be lost.
	 */
	public void resaveData() {
		for (EventPlayer all: players) {
			removelist.addToListWithoutSave(all.getPlayer());
		}
		removelist.saveData();
	}

	/**
	 * This method deletes all current EventPlayer-instances and recreates them for every player.
	 * This is used to switch to custom EventPlayer-classes when a new plugin is attached.
	 * Teams, spawn groups and lives will be copied to the new instances.
	 */
	public void recreatePlayerInstances() {
		EventPlayer[] all = getAllPlayers();
		players.clear();
		getWarp().setEventsSuppressed(true);
		for (EventPlayer p: all) {
			EventPlayer pnew = addPlayerToList(p.getPlayer());
			pnew.setTeam(p.getTeam());
			pnew.setSpawngroup(p.getSpawngroup());
			pnew.setLives(p.getLives());
		}
		getWarp().setEventsSuppressed(false);
	}
	
	/**
	 * Returns the EventPlayer with this Player-instance if he exists
	 * @param p The Player-instance
	 * @return The EventPlayer-instance or null, if this PlayerManager
	 * doesn't control a this player
	 */
	public EventPlayer getPlayer(Player p) {
		for (EventPlayer all: players) {
			if (all.getPlayer() == p) {
				return all;
			}
		}
		return null;
	}
	/**
	 * Returns the EventPlayer with this name if he exists
	 * This basically performs Bukkit.getPlayerExact(name) and
	 * then calls getPlayer(Player)
	 * @param name The name of the EventPlayer
	 * @return The EventPlayer-instance or null, if this PlayerManager
	 * doesn't control a player with the given name
	 */
	public EventPlayer getPlayer(String name) {
		Player p = Bukkit.getPlayerExact(name);
		if (p == null) return null;
		return getPlayer(p);
	}

	/**
	 * Returns an Array containing all EventPlayers
	 * this PlayerManager is taking care of.
	 * @return An Array containing all EventPlayers this
	 * PlayerManager is taking care of.
	 */
	public EventPlayer[] getAllPlayers() {
		EventPlayer[] list = new EventPlayer[players.size()];
		return players.toArray(list);
	}
	
	/**
	 * Gets all players that are in the given team.
	 * @return A list of all EventPlayers that are part of the given team.
	 */
	public EventPlayer[] getPlayersByTeam(Team team) {
		ArrayList<EventPlayer> list = new ArrayList<>(players.size());
		for (EventPlayer all: players) {
			if (all.getTeam() == team) {
				list.add(all);
			}
		}
		return list.toArray(new EventPlayer[list.size()]);
	}

	protected EventwarpMain getMain() {
		return main;
	}


	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		try {
			EventPlayer p = getPlayer(e.getPlayer());
			if (p != null) kickPlayer(p);
		} catch (Exception ex) {
			main.getErrorHandler().ListenerExceptionOccured(ex);
		}
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		try {
			if (e.getEntity() instanceof Player) {
				EventPlayer p = getPlayer((Player) e.getEntity());
				if (p != null) {
					Player dmgplayer = CodeTools.getPlayerDamager(e);
					if (dmgplayer != null) {
						EventPlayer damager = getPlayer(dmgplayer);
						if (damager != null) {
							p.registerDamage(damager);
						}
					}
				}
			}
		} catch (Exception ex) {
			main.getErrorHandler().ListenerExceptionOccured(ex);
		}

	}
	
	/**
	 * This method returns a suitable respawn location for the given EventPlayer.
	 * The Location is chosen the following way:
	 *   - If the lobby mode is active, a lobby spawnpoint is returned
	 *   - Else if the player is in spectator mode, a spectator spawnpoint is returned
	 *   - Else a spawnpoint of the player's spawn group is returned.
	 * If the chosen spawn group (by the explained way) does not contain any spawnpoints, a spawnpoint
	 * of the default player group will be returned.
	 */
	Location getRespawnLocation(EventPlayer p) {
		Location loc;
		if (warp.isLobbyMode()) {
			loc = warp.getSpawnManager().getRandomLocation(SpawnLocationManager.LOBBY_SPAWN_GROUP);
		} else if (p.isSpectatormode()) {
			loc = warp.getSpawnManager().getRandomLocation(SpawnLocationManager.DEFAULT_SPECTATOR_SPAWN_GROUP);
		} else {
			loc = warp.getSpawnManager().getRandomLocation(p.getSpawngroup());
		}
		if (loc == null) {
			loc = warp.getSpawnManager().getRandomLocation(SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP);
		}
		return loc;
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		EventPlayer p = getPlayer(e.getPlayer());
		if (p != null) {
			e.setRespawnLocation(getRespawnLocation(p));
			p.respawn(false);
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onDeath(PlayerDeathEvent e) {
		EventPlayer p = getPlayer(e.getEntity());
		if (p != null) {
			e.setDroppedExp(0);
			e.setKeepLevel(true);
			notifyDeath(p);
		}
	}
	
	/**
	 * This method notifies all Listeners to the Eventwarp
	 * that a player left the Eventwarp
	 * @param p The player who left the Eventwarp
	 */
	void notifyLeave(EventPlayer p) {
		for (EventwarpListener all: warp.getListeners()) {
			all.onLeave(p);
		}
	}
	/**
	 * This method notifies all Listeners to the Eventwarp
	 * that a player at the Eventwarp just respawned.
	 * @param p The player who just respawned.
	 */
	public void notifyRespawn(EventPlayer p) {
		for (EventwarpListener all: warp.getListeners()) {
			all.onRespawn(p);
		}
	}
	private void notifyJoin(EventPlayer p) {
		for (EventwarpListener all: warp.getListeners()) {
			all.onJoin(p);
		}
	}
	private void notifyDeath(EventPlayer p) {
		for (EventwarpListener all: warp.getListeners()) {
			all.onDeath(p);
		}
	}
	
	protected Eventwarp getWarp() {
		return warp;
	}
	public VisualPlayerList getGUI() {
		return gui;
	}
	public SpectatorManager getSpectatorManager() {
		return spectatorManager;
	}

	/**
	 * Returns the amount of currently active teams. The active teams are those that come first in the
	 * Teams.values() array.
	 */
	public int getActiveTeams() {
		return activeTeams;
	}

}
