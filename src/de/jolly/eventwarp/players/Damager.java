package de.jolly.eventwarp.players;

/**
 * This interface represents a player who
 * inflicted damage to another player.
 * @author _Jolly_
 *
 */
public interface Damager {

}
