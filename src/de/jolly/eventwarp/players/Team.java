package de.jolly.eventwarp.players;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;

/**
 * The Team enum. Stolen from TeamPvP's TeamColor with little
 * modifications. Yeah, I was lazy. Sorry.
 * 
 * @author _Jolly_
 *
 */
public enum Team {

	BLUE	(ChatColor.BLUE,		"blue",		"�9Blau",	Material.BLUE_WOOL,	Color.BLUE,		"team_blue"),
	GREEN	(ChatColor.GREEN,		"green",	"�aGr�n",	Material.LIME_WOOL,	Color.GREEN,	"team_green"),
	RED		(ChatColor.RED,			"red",		"�cRot",	Material.RED_WOOL,	Color.RED,		"team_red"),
	YELLOW	(ChatColor.YELLOW,		"yellow",	"�eGelb",	Material.YELLOW_WOOL,	Color.YELLOW,	"team_yellow"),
	PURPLE	(ChatColor.DARK_PURPLE,	"purple",	"�5Lila",	Material.PURPLE_WOOL,	Color.PURPLE,	"team_purple"),
	WHITE	(ChatColor.WHITE,		"white",	"�fWei�",	Material.WHITE_WOOL,	Color.WHITE,	"team_white");

	private final ChatColor chatcolor;
	private final String name;
	private final String chatname;
	private final Material woolColorType;
	private final Color armorColor;
	private final String spawngroup;

	private Team(ChatColor chatcolor, String name, String chatname, Material woolColorType, Color armorColor, String spawngroup) {
		this.chatcolor = chatcolor;
		this.name = name;
		this.chatname = chatname;
		this.woolColorType = woolColorType;
		this.armorColor = armorColor;
		this.spawngroup = spawngroup;
	}

	public ChatColor getChatColor() {
		return chatcolor;
	}

	public String getName() {
		return name;
	}

	public String getChatName() {
		return chatname;
	}

	public Material getWoolColorType() {
		return woolColorType;
	}

	public Color toColor() {
		return armorColor;
	}
	
	public String getDefaultSpawngroup() {
		return spawngroup;
	}

	public static Team fromCharacter(char c) {
		Team color;
		switch (c) {

		case 'b':
			color = Team.BLUE;
			break;
		case 'g':
			color = Team.GREEN;
			break;
		case 'r':
			color = Team.RED;
			break;
		case 'y':
			color = Team.YELLOW;
			break;
		case 'p':
			color = Team.PURPLE;
			break;
		case 'w':
			color = Team.WHITE;
			break;
		default:
			color = null;
			break;
		}
		return color;
	}

}
