package de.jolly.eventwarp.players;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

import de.jolly.eventwarp.external.EventwarpListener;

/**
 * This class takes care of the Spectators. It does everything
 * which applies to spectators and which is not controlled by a Setting.
 */
public class SpectatorManager implements Listener, EventwarpListener {
	
	private final PlayerManager manager;
	private final Plugin main;
	
	SpectatorManager(PlayerManager manager, Plugin main) {
		this.main = main;
		this.manager = manager;
	}
	
	/**
	 * Reloads the Invisibility all Spectators have.
	 * What I meant to say is: This turns all Spectators invisible again, but
	 * only towards other EventPlayers and it makes the Spectators see each other and all participiants.
	 * It was created to avoid conflicts with the InvisibilitySetting
	 */
	public void reloadInvisibility() {
		EventPlayer[] all = manager.getAllPlayers();
		EventPlayer[] invisible = getSpectators();
		
		for (EventPlayer p: invisible) {
			for (EventPlayer ep: all) {
				if (!ep.isSpectatormode()) {
					ep.getPlayer().hidePlayer(main, p.getPlayer());
				}
				p.getPlayer().showPlayer(main, ep.getPlayer());
			}
		}
	}
	
	/**
	 * Internal method to enable Spectatormode
	 */
	void enableSpectatormode(EventPlayer p) {
		Player player = p.getPlayer();
		EventPlayer[] all = manager.getAllPlayers();
		for (EventPlayer ep: all) {
			if (!ep.isSpectatormode()) {
				ep.getPlayer().hidePlayer(main, player);
			}
			player.showPlayer(main, ep.getPlayer());
		}
		p.getPlayer().setAllowFlight(true);
	}
	/**
	 * Internal method to disable Spectatormode
	 */
	void disableSpectatormode(EventPlayer p) {
		Player player = p.getPlayer();
		EventPlayer[] all = manager.getAllPlayers();
		for (EventPlayer ep: all) {
			if (!ep.isSpectatormode()) {
				ep.getPlayer().showPlayer(main, player);
			} else {
				player.hidePlayer(main, ep.getPlayer());
			}
		}
		p.getPlayer().setAllowFlight(false);
	}
	
	public EventPlayer[] getSpectators() {
		Set<EventPlayer> all = new HashSet<>();
		for (EventPlayer p: manager.getAllPlayers()) {
			if (p.isSpectatormode()) {
				all.add(p);
			}
		}
		return all.toArray(new EventPlayer[all.size()]);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void disableDamage(EntityDamageEvent e) {
		if (!(e.getEntity() instanceof Player)) {
			return;
		}
		EventPlayer p = manager.getPlayer((Player) e.getEntity());
		if (p != null && p.isSpectatormode()) {
			e.setCancelled(true);
		}
	}

	@Override
	public void onJoin(EventPlayer player) {
		for (EventPlayer s: getSpectators()) {
			player.getPlayer().hidePlayer(main, s.getPlayer());
		}
	}

	@Override
	public void onLeave(EventPlayer player) {
		if (player.isSpectatormode()) {
			player.getPlayer().setAllowFlight(false);
		}
	}

	@Override
	public void onRespawn(EventPlayer player) {
	}

	@Override
	public void onDeath(EventPlayer player) {
	}
}
