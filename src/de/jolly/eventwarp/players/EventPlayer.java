package de.jolly.eventwarp.players;

import java.util.List;

import org.bukkit.Color;
import org.bukkit.entity.Player;

/**
 * EventPlayers are the players that are currently at the Eventwarp.
 * It is rarely appropriate to directly implement this interface.
 * (Believe me, I really do NOT reccommend you to implement this interface directly.)
 * If you want to create your own EventPlayer-class to replace the default one
 * used by the Eventwarp, consider just deriving it from DefaultEventPlayer, which
 * is the class the EventPlayer uses itself by default, which allows you to override
 * certain methods without having to reimplement all the methods needed by this interface
 * by yourself.
 * Apart from that you'd have to call every Event that is triggered by the class
 * DefaultEventPlayer yourself in order to maintain full functionality.
 * @author _Jolly_
 *
 */
public interface EventPlayer extends Damager {
	
	/**
	 * Returns the Player instance this EventPlayer
	 * is referring to.
	 * @return The Player instance of this EventPlayer
	 */
	public Player getPlayer();
	/**
	 * Returns the name of this EventPlayer.
	 * This generally should return the value of getPlayer().getName()
	 * @return The name of this EventPlayer.
	 */
	public String getName();
	
	/**
	 * Kicks this Player from the Eventwarp
	 */
	public void kick();
	/**
	 * Bans this Player from the Eventwarp
	 */
	public void ban();
	
	/**
	 * Respawns the Player in the game.
	 * @param teleport if the player shall be teleported
	 */
	public void respawn(boolean teleport);
	/**
	 * Resets lives, disables Spectatormode and respawns the player.
	 */
	public void reset();
	
	/**
	 * Sends a message to this EventPlayer
	 * @param message The message to be sent
	 */
	public void sendMessage(String message);
	
	/**
	 * Returns the amount of lives this EvenPlayer has.
	 */
	public int getLives();
	/**
	 * Updates the amount of lives this EventPlayer has.
	 * @param lives The new amount of lives
	 */
	public void setLives(int lives);
	
	/**
	 * Enables the Spectatormode for this player
	 */
	public void enableSpectatormode();
	/**
	 * Disables the Spectatormode for this player
	 */
	public void disableSpectatormode();
	/**
	 * Returns whether or not this player is in spectatormode
	 */
	public boolean isSpectatormode();
	/**
	 * Colors the leather armor of this player, given he is wearing any.
	 */
	public void setClothesColor(Color color);
	/**
	 * Returns a list containing Strings which hold information
	 * about the player's status. This list is used as lore for the PlayerButtons
	 * in the PlayerManager GUI and for the player status command.
	 */
	public List<String> getStatus();
	/**
	 * Returns the name of the spawn group this player belongs to.
	 * A spawn group determines what kind of spawnpoints are available to this player.
	 * For example, different teams have different spawnpoints.
	 */
	public String getSpawngroup();
	/**
	 * Sets the spawn group for this player.
	 * @param spawngroup The new spawn group this player will be assigned to.
	 */
	public void setSpawngroup(String spawngroup);
	/**
	 * Returns this player's team.
	 * @return The Team this EventPlayer belongs to or null, if he is not part of a team.
	 */
	public Team getTeam();
	/**
	 * Sets the Team of this EventPlayer.
	 * @param team The new Team of this EventPlayer.
	 */
	public void setTeam(Team team);
	
	/**
	 * Returns whoever inflicted the last damage on this Player.
	 * However this returns null if the time since when the damage was
	 * inflicted is above a certain border (5000 millisec. by default)
	 * @return The Player who inflicted the last damage on this Player or null,
	 * if no damage inflict was registered or the last damage infliction was too long ago.
	 */
	public Damager getLastDamager();
	
	/**
	 * Registers that this EventPlayer got damaged by the given Damager
	 * @param d The damage who infliced damage to this EventPlayer
	 */
	public void registerDamage(Damager d);
}
