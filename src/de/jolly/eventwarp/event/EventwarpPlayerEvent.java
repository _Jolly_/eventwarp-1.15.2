package de.jolly.eventwarp.event;

import de.jolly.eventwarp.controls.EventwarpEvent;
import de.jolly.eventwarp.players.EventPlayer;

public abstract class EventwarpPlayerEvent extends EventwarpEvent {
	
	private EventPlayer player;
	
	public EventwarpPlayerEvent(EventPlayer player) {
		this.player = player;
	}
	
	/**
	 * Returns the player who has triggered this Event.
	 */
	public EventPlayer getPlayer() {
		return player;
	}
	
}
