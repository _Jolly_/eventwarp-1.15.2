package de.jolly.eventwarp.event;

import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.controls.EventwarpEvent;

public class EventwarpToggleLobbymodeEvent extends EventwarpEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final boolean lobbymode;
	
	public EventwarpToggleLobbymodeEvent(boolean lobbymode) {
		super();
		this.lobbymode = lobbymode;
	}
	
	/**
	 * @return true, if the lobby mode was enabled, false if it was disabled.
	 */
	public boolean getLobbymodeEnabled() {
		return lobbymode;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
