package de.jolly.eventwarp.event;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.controls.EventwarpEvent;

/**
 * This Event is triggered when the Eventwarp opens.
 * Cancelling it will cause the Eventwarp to not open.
 *
 */
public class EventwarpOpenEvent extends EventwarpEvent implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	private boolean cancelled;
	
	public EventwarpOpenEvent() {
		super();
		cancelled = false;
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

}
