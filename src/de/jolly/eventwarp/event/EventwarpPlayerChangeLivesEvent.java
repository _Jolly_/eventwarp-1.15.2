package de.jolly.eventwarp.event;

import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.players.EventPlayer;

/**
 * This Event is triggered when the amount of lives a player has changes.
 * It is called before the lives are updated.
 * @author _Jolly_
 *
 */
public class EventwarpPlayerChangeLivesEvent extends EventwarpPlayerEvent implements Cancellable {
	
	private int change;
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();
	
	public EventwarpPlayerChangeLivesEvent(EventPlayer player, int change) {
		super(player);
		this.change = change;
		cancelled = false;
	}
	
	/**
	 * The change that is applied to the lives.
	 * Negative, if lives are deducted, else positive
	 */
	public int getChange() {
		return change;
	}
	/**
	 * Returns the amount of lives this player will have
	 * after the change.
	 */
	public int getResultingLives() {
		return getPlayer().getLives()+change;
	}
	
	/**
	 * Sets whether or not the Event shall be cancelled.
	 * If the Event is cancelled, the player's lives will not change.
	 */
	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	@Override
	public boolean isCancelled() {
		return cancelled;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
