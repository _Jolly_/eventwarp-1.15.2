package de.jolly.eventwarp.event;

import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.players.EventPlayer;

public class EventwarpPlayerEnterSpectatormodeEvent extends EventwarpPlayerEvent {
	
	private static final HandlerList handlers = new HandlerList(); 
	
	public EventwarpPlayerEnterSpectatormodeEvent(EventPlayer player) {
		super(player);
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
