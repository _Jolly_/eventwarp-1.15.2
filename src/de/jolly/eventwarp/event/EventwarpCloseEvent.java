package de.jolly.eventwarp.event;

import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.controls.EventwarpEvent;

/**
 * This Event is triggered when the Eventwarp closes.
 *
 */
public class EventwarpCloseEvent extends EventwarpEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	public EventwarpCloseEvent() {
		super();
	}
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}

