package de.jolly.eventwarp.event;

import org.bukkit.event.HandlerList;

import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.Team;

/**
 * This Event is called after a player has switched his team and hence is not Cancellable.
 * @author _Jolly_
 *
 */
public class EventwarpPlayerSwitchTeamEvent extends EventwarpPlayerEvent {
	
	private final Team old;
	
	public EventwarpPlayerSwitchTeamEvent(EventPlayer player, Team old) {
		super(player);
		this.old = old;
	}
	
	/**
	 * Returns the Team this player was in before.
	 */
	public Team getOldTeam() {
		return old;
	}
	
	private static final HandlerList handlers = new HandlerList();
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}

}
