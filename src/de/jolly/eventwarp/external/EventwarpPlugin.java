package de.jolly.eventwarp.external;

public interface EventwarpPlugin {
	
	/**
	 * Returns the EventwarpAdapter for this plugin.
	 * An EventwarpAdapter is neccessary for an EventwarpPlugin.
	 * @return The EventwarpAdapter of this plugin.
	 */
	public EventwarpAdapter getEventwarpAdapter();
	
}
