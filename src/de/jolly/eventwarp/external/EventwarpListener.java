package de.jolly.eventwarp.external;

import de.jolly.eventwarp.players.EventPlayer;

public interface EventwarpListener {
	
	public void onJoin(EventPlayer player);

	public void onLeave(EventPlayer player);

	public void onRespawn(EventPlayer player);
	
	public void onDeath(EventPlayer player);
	
}
