package de.jolly.eventwarp.external;

import org.bukkit.entity.Player;

import codetools.main.EnhancedJavaPlugin;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;

/**
 * The EventwarpAdapter is used to receive incoming messages from the Eventwarp
 * and to extend the Actions on the Warppoint.
 * @author _Jolly_
 *
 */
public interface EventwarpAdapter extends EventwarpListener {
	
	/**
	 * Creates the EventPlayer instance.
	 * Use this to make the Eventwarp use your own custom EventPlayer class.
	 * If you don't want to make a specific EventPlayer class, just return null in this method.
	 * @param p The player for whom the EventPlayer instance shall be created.
	 * @param manager The PlayerManager of the Eventwarp.
	 */
	public EventPlayer createEventPlayer(Player p, PlayerManager manager);
	
	/**
	 * This method will be called when an Eventwarp is being attached to this
	 * EventwarpAdapter. This method is called before onReload(), so the Eventwarp
	 * will still be in its old state in terms of settings etc.
	 * @param warp The warp which is being attached.
	 */
	public void onAttach(Eventwarp warp);
	
	/**
	 * This method will be called when this EventwarpAdapter is being detached
	 * from the Eventwarp.
	 * This method is called before this Listener is unregistered so if other
	 * update messages occur during the detaching process they will still be sent
	 * to this EventwarpAdapter.
	 */
	public void onDetach();
	
	/**
	 * This method will be called when the Eventwarp fully reloads.
	 * This includes when your adapter is attached to the Eventwarp.
	 * Use this method to register all of your listeners.
	 */
	public void onReload();
	
	/**
	 * Returns the Main plugin class of the plugin to which this Adapter belongs
	 * @return The Main class of this Adapter's plugin
	 */
	public EnhancedJavaPlugin getMain();
	
}
