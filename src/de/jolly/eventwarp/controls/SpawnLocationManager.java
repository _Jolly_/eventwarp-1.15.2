package de.jolly.eventwarp.controls;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import codetools.main.AdvancedJavaPlugin;
import codetools.main.CodeTools;

/**
 * This manages the Locations of the Eventwarp's spawnpoints.
 * @author _Jolly_
 *
 */
public class SpawnLocationManager {

	private final AdvancedJavaPlugin main;
	private final Eventwarp warp;

	/**
	 * This Map holds for each String (spawngroup) a list
	 * of spawnpoints that belong to it.
	 * A spawngroup can represent a team, the spectators, or the normal players.
	 */
	private final Map<String, List<Location>> spawnpoints;
	
	public static final String DEFAULT_PLAYER_SPAWN_GROUP = "players";
	public static final String DEFAULT_SPECTATOR_SPAWN_GROUP = "spectators";
	public static final String LOBBY_SPAWN_GROUP = "lobby";
	
	/**
	 * The path in the FileConfiguration where the spawnpoints are saved.
	 */
	private final String configpath;

	private final Random r;

	public SpawnLocationManager(AdvancedJavaPlugin main, Eventwarp warp) {
		this.main = main;
		this.warp = warp;
		configpath = "spawnlocations";
		r = new Random();
		spawnpoints = new HashMap<>();
	}
	public SpawnLocationManager(AdvancedJavaPlugin main, Eventwarp warp, String configpath) {
		this.main = main;
		this.warp = warp;
		this.configpath = configpath;
		r = new Random();
		spawnpoints = new HashMap<>();
	}

	private List<Location> getArray(String spawngroup) {
		return spawnpoints.get(spawngroup);
	}

	public void addLocation(Location loc, String spawngroup) {
		addLocationInternal(loc, spawngroup);
		save();
	}
	private void addLocationInternal(Location loc, String spawngroup) {
		List<Location> spawnpoints = getArray(spawngroup);
		if (spawnpoints == null) {
			spawnpoints = new ArrayList<>();
			this.spawnpoints.put(spawngroup, spawnpoints);
		}
		if (spawnpoints.contains(loc)) {
			return;
		}
		spawnpoints.add(loc);
	}
	public void addLocations(Location[] locs, String spawngroup) {
		for (Location l: locs) {
			addLocationInternal(l, spawngroup);
		}
		save();
	}
	public void removeLocation(int index, String spawngroup) {
		List<Location> spawnpoints = getArray(spawngroup);
		if (spawnpoints == null) {
			return;
		}
		spawnpoints.remove(index);
		if (spawnpoints.isEmpty()) {
			this.spawnpoints.remove(spawngroup);
		}

		save();
	}
	/**
	 * Returns true if the given spawngroup has at least one spawnpoint.
	 * If the given spawngroup is not defined, false is returned.
	 */
	public boolean hasLocations(String spawngroup) {
		return getArray(spawngroup) != null;
	}

	public Location getLocation(String spawngroup, int index) {
		List<Location> spawnpoints = getArray(spawngroup);
		if (spawnpoints == null) return null;
		return spawnpoints.get(index);
	}
	/**
	 * Returns a random spawnpoint of those saved here.
	 * @return A random Location out of those that are storaged in this class
	 * or null, if no spawnpoint is storaged here.
	 */
	public Location getRandomLocation(String spawngroup) {
		List<Location> spawnpoints = getArray(spawngroup);
		if (spawnpoints == null) {
			return null;
		}

		return spawnpoints.get(r.nextInt(spawnpoints.size()));
	}

	public Location[] getAllLocations(String spawngroup) {
		List<Location> spawnpoints = getArray(spawngroup);
		if (spawnpoints == null) return new Location[0];
		Location[] value = new Location[spawnpoints.size()];
		return spawnpoints.toArray(value);
	}

	void save() {
		File file = warp.getCurrentEventFile();
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

		for (String spawngroup: spawnpoints.keySet()) {
			List<Location> spawnpoints = this.spawnpoints.get(spawngroup);
			cfg.set(configpath+"."+spawngroup+".size", spawnpoints.size());
			for (int i = 0; i < spawnpoints.size(); i++) {
				CodeTools.saveLocation(spawnpoints.get(i), cfg, configpath+"."+spawngroup+".elements."+i);
			}
		}

		try {
			cfg.save(file);
		} catch (IOException e) {
			main.getErrorHandler().ExceptionOccured(e);
		}

	}
	void load() {
		spawnpoints.clear();

		File file = warp.getCurrentEventFile();
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		
		if (cfg.getConfigurationSection(configpath) == null) cfg.createSection(configpath);
		Map<String, Object> entries = cfg.getConfigurationSection(configpath).getValues(false);
		for (String spawngroup: entries.keySet()) {
			List<Location> spawns = new ArrayList<>();
			ConfigurationSection section = (ConfigurationSection) entries.get(spawngroup);
			int size = section.getInt("size");
			for (int i = 0; i < size; i++) {
				spawns.add(CodeTools.loadLocation(section, "elements."+i));
			}
			spawnpoints.put(spawngroup, spawns);
		}
	}
}
