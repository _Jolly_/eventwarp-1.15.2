package de.jolly.eventwarp.controls;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;

public class UndoButton extends ItemButton {
	
	private final boolean kit;
	private final EventEquipment equipment;
	
	/**
	 * Creates the UndoButton
	 * @param kit true, if the inventory the button is used for is the kit inventory, false, if it is the armor inventory.
	 */
	UndoButton(EventEquipment equipment, boolean kit, InteractionInventory inv) {
		super(Material.RED_WOOL, "�cR�ckg�ngig", inv);
		this.equipment = equipment;
		this.kit = kit;
		setTask(task);
	}
	
	private final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			InteractionInventory inv;
			ItemStack[] newContents;
			if (kit) {
				inv = UndoButton.this.equipment.getKitInventory();
				newContents = UndoButton.this.equipment.getKit();
			} else {
				inv = UndoButton.this.equipment.getArmorInventory();
				newContents = UndoButton.this.equipment.getArmor();
			}
			for (int i = 0; i < newContents.length; i++) {
				inv.setItem(newContents[i], i);
			}
			inv.update();
		}
	};
}
