package de.jolly.eventwarp.controls;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

import codetools.main.AdvancedJavaPlugin;
import de.jolly.eventwarp.event.EventwarpCloseEvent;
import de.jolly.eventwarp.event.EventwarpOpenEvent;
import de.jolly.eventwarp.event.EventwarpToggleLobbymodeEvent;
import de.jolly.eventwarp.external.EventwarpAdapter;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.EventwarpSettings;

/**
 * The class Eventwarp represents an Eventwarp-instance. (Yeah that makes sense, huh?)
 * By using this class you can access controls to any function the Eventwarp offers, like
 * adding players using the PlayerManager, kicking players, changing the warppoint's location
 * or any other setting.
 * @author _Jolly_
 *
 */
public class Eventwarp {

	private final String FILENAME = "eventwarp.yml";
	private final String LOBBY_FILENAME = "eventwarplobby.yml";

	/**
	 * The Main Plugin class
	 */
	private EventwarpMain main;
	/**
	 * This Eventwarp's PlayerManager
	 */
	private PlayerManager playerManager;
	/**
	 * The EventwarpAdapter that is currently attached to this Eventwarp.
	 * If no certain Adapter is attached, this refers to a default Adapter.
	 */
	private EventwarpAdapter adapter;
	/**
	 * The SpawnLocationManager of this Eventwarp
	 */
	private SpawnLocationManager spawnManager;
	/**
	 * The Settings
	 */
	private EventwarpSettings settings;
	/**
	 * The Equipment
	 */
	private EventEquipment equipment;
	/**
	 * This Eventwarp's log
	 */
	private EventwarpLog log;
	/**
	 * The Listeners for this Eventwarp
	 */
	private Set<EventwarpListener> listeners;

	/**
	 * Defines whether or not the Eventwarp is open.
	 */
	private boolean open;
	
	/**
	 * Defines whether or not all current EventwarpEvents will
	 * not be sent.
	 */
	private boolean eventsSuppressed;
	/**
	 * If the Eventwarp is currently in lobby mode
	 */
	private boolean lobbyMode;
	
	public Eventwarp(EventwarpMain main) {
		this.main = main;

		listeners = new HashSet<>();
		eventsSuppressed = false;
		lobbyMode = false;
		
		//The order here is important
		adapter = new DefaultAdapter(main);//The adapter is needed to correctly access the eventwarp.yml file
		settings = new EventwarpSettings(this, main);//The Settings have to be loaded in order to be able to access the Panel
		spawnManager = new SpawnLocationManager(main, this);//Now we can load the SpawnManager, the EventEquipment and the PlayerManager
		equipment = new EventEquipment(this, main, main.getErrorHandler());
		playerManager = new PlayerManager(this, main);
		log = new EventwarpLog(this);
		main.getServer().getPluginManager().registerEvents(log, main);
		reloadAll();//Last, everything gets loaded
	}

	/**
	 * Attaches a new EventwarpAdapter to this Eventwarp.
	 * Only one Adapter is allowed at a time. If this method is used when
	 * there is already another adapter attached, the current adapter will be
	 * detached.
	 * This method fails silently if null is passed as an argument.
	 * @param adapter The new EventwarpAdapter for this Eventwarp
	 */
	public void attachAdapter(EventwarpAdapter adapter) {
		if (adapter != null) {

			setOpen(false);	

			saveConfiguration();
			this.adapter.onDetach();
			this.adapter = adapter;
			adapter.onAttach(this);
			reloadAll();
			
			getLog().log("Attached new Adapter: �3"+adapter.getClass());
		}
	}
	/**
	 * Detaches the current EventwarpAdapter.
	 * This will cause the Eventwarp to switch back to the default Adapter
	 * and stop sending update messages to the old EventwarpAdapter.
	 */
	public void detachAdapter() {
		attachAdapter(new DefaultAdapter(main));
	}

	private void saveConfiguration() {
		spawnManager.save();
		equipment.save();
		settings.save();
	}
	private void reloadAll() {
		listeners.clear();
		reloadConfiguration();
		playerManager.recreatePlayerInstances();
		registerListener(playerManager.getGUI());
		registerListener(playerManager.getSpectatorManager());
		playerManager.getGUI().reload();
		registerListener(log);
		registerListener(adapter);
		adapter.onReload();
	}
	private void reloadConfiguration() {
		spawnManager.load();
		equipment.load();
		settings.reloadAll();
	}

	/**
	 * Calls an EventwarpEvent.
	 * Use this method for any Event called on this Eventwarp, as the Eventwarp registers itself
	 * at the Event to make sure that Listeners know on which Eventwarp-instance the Event was
	 * triggered. (If the Eventwarp will ever support more than one instance)
	 * This method also catches EventExceptions thrown during the process and sends them to the most
	 * appropriate ErrorHandler available to make sure the current process keeps running.
	 * If the Eventwarp is set to suppress Events, this method will quit without notifying listeners.
	 * @return False, if the Event was cancelled, true otherwise. If the given Event isn't Cancellable, this method
	 * will always return true.
	 * If the Eventwarp is set to suppress Events, true is returned aswell.
	 */
	public boolean callEvent(EventwarpEvent e) {
		if (e == null || eventsSuppressed) {
			return true;
		}
		
		e.setEventwarp(this);
		try {
			Bukkit.getPluginManager().callEvent(e);
		} catch (Exception ex) {
			if (getAdapter().getMain() instanceof AdvancedJavaPlugin) {
				((AdvancedJavaPlugin)getAdapter().getMain()).getErrorHandler().ListenerExceptionOccured(ex);
			} else {
				main.getErrorHandler().ListenerExceptionOccured(ex);
			}
		}

		if (e instanceof Cancellable) {
			return !((Cancellable)e).isCancelled();
		}

		return true;

	}
	
	/**
	 * Sets whether or not all Events that are triggered using callEvent on THIS class shall not be triggered.
	 * As a result, Listeners will not be notified that the given Event occured.
	 * So only use this method if you really do not want Listeners to know that something happens.
	 * Consider that actions of Listeners might be needed for proper functionality.
	 * Also if you use this method, think of disabling suppression when you are done with whatever you are doing.
	 */
	public void setEventsSuppressed(boolean suppressed) {
		eventsSuppressed = suppressed;
	}
	public boolean areEventsSuppressed() {
		return eventsSuppressed;
	}
	
	/**
	 * Toggles the lobby mode.
	 */
	public void setLobbyMode(boolean lobbyMode) {
		if (this.lobbyMode == lobbyMode) return;
		if (lobbyMode) {
			this.lobbyMode = true;
			getLog().log("Enabling Lobbymode.");
		} else {
			this.lobbyMode = false;
			getLog().log("Disabling Lobbymode.");
		}
		callEvent(new EventwarpToggleLobbymodeEvent(lobbyMode));
		playerManager.resetAllPlayers();
		if (lobbyMode) getLog().log("Lobby Mode enabled. Lobby Settings loaded.");
		else getLog().log("Lobby Mode disabled. Game Settings loaded.");
	}
	public boolean isLobbyMode() {
		return lobbyMode;
	}
	/**
	 * Sets whether or not the Eventwarp shall be open
	 */
	public void setOpen(boolean open) {
		if (this.open == open) {
			return;
		}

		if (open) {
			if (callEvent(new EventwarpOpenEvent())) {
				this.open = true;
			} else {
			}
		} else {
			this.open = false;
			callEvent(new EventwarpCloseEvent());
		}
		
	}
	/**
	 * Returns true, if the Eventwarp is currently open, false otherwise
	 */
	public boolean isOpen() {
		return open;
	}

	public PlayerManager getPlayerManager() {
		return playerManager;
	}
	public EventwarpAdapter getAdapter() {
		return adapter;
	}
	/**
	 * Returns whether the default EventwarpAdapter is currently attached.
	 */
	public boolean isDefaultAdapter() {
		return (getAdapter().getClass() == DefaultAdapter.class);
	}
	/**
	 * Returns the SpawnManager, which can be used to organize spawnpoints of the different groups of players.
	 */
	public SpawnLocationManager getSpawnManager() {
		return spawnManager;
	}
	/**
	 * Returns the EventwarpSettings. Here you can register your own settings and to some extent control existing settings.
	 */
	public EventwarpSettings getSettings() {
		return settings;
	}
	/**
	 * Returns the EventEquipment, where you can specify the equipment players at the Eventwarp receive.
	 */
	public EventEquipment getEventEquipment() {
		return equipment;
	}
	/**
	 * Returns the EventwarpLog, where you can log anything you deem important.
	 */
	public EventwarpLog getLog() {
		return log;
	}
	/**
	 * Returns the Plugin main class of the Eventwarp. Intended for internal use to register events.
	 * You could use it to access the Eventwarp's banlist, but that's basically all.  
	 */
	public EventwarpMain getEventwarpMain() {
		return main;
	}

	/**
	 * Returns the File which the current Eventwarp's configuration shall be saved
	 * or loaded from. It depends on the attached Adapter and on whether or not the game is in lobby mode.
	 */
	public File getCurrentEventFile() {
		if (lobbyMode) {
			return adapter.getMain().getPluginFile(LOBBY_FILENAME);
		} else {
			return adapter.getMain().getPluginFile(FILENAME);
		}
	}

	public void registerListener(EventwarpListener listener) {
		listeners.add(listener);
	}
	public void unregisterListener(EventwarpListener listener) {
		listeners.remove(listener);
	}
	public EventwarpListener[] getListeners() {
		EventwarpListener[] list = new EventwarpListener[listeners.size()];
		return list = listeners.toArray(list);
	}
}
