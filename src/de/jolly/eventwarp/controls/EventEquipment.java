package de.jolly.eventwarp.controls;

import java.io.File;
import java.io.IOException;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import codetools.errorhandling.ErrorHandler;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import codetools.main.CodeTools;
import de.jolly.eventwarp.event.EventwarpToggleLobbymodeEvent;
import de.jolly.eventwarp.settings.ReturnToPanelButton;

/**
 * This class is from the OITC-Plugin (which itself stole it from the Winterevent) with little changes to the
 * way the data is saved.
 * @author _Jolly_
 *
 */
public class EventEquipment implements Listener {


	/**
	 * Die Referenz zum Eventwarp
	 */
	private final Eventwarp warp;
	/**
	 * The ErrorHandler
	 */
	protected final ErrorHandler errorHandler;

	/**
	 * Die gespeicherte Ausr�stung f�r die Spieler
	 */
	private ItemStack[][] equipment;

	/**
	 * Der Name des Inventars, mit dem man das Kit einstellen kann
	 */
	public static final String KitInvName = "�bEventwarp - Kit";
	/**
	 * Der Name des Inventars, mit dem man die Ausr�stung einstellen kann
	 */
	public static final String ArmorInvName = "�bEventwarp - R�stung";

	private final InteractionInventory kit;
	private final InteractionInventory armor;

	private final String DEFAULT_PATH = "equipment";
	private final String LOBBYMODE_PATH = "lobbyequipment";
	private String path;

	public EventEquipment(Eventwarp warp, Plugin main, ErrorHandler handler) {
		this.warp = warp;
		errorHandler = handler;
		updatePath();

		//Creating the KitInventory
		kit = new InteractionInventory(45, KitInvName);
		kit.setCancelAlways(false);
		appendHotbar(kit, true);

		//Creating the ArmorInventory
		armor = new InteractionInventory(18, ArmorInvName);
		armor.setCancelAlways(false);
		appendHotbar(armor, false);

		main.getServer().getPluginManager().registerEvents(this, main);
	}

	private void updatePath() {
		if (warp.isLobbyMode()) {
			path = LOBBYMODE_PATH;
		} else {
			path = DEFAULT_PATH;
		}
	}

	private void appendHotbar(InteractionInventory inv, boolean kit) {
		ItemButton returnbutton = new ReturnToPanelButton(inv, warp.getSettings().getControlPanel());
		inv.setItem(returnbutton, inv.getSize()-9);
		ItemButton undo = new UndoButton(this, kit, inv);
		inv.setItem(undo, inv.getSize()-8);
		ItemButton empty = new ItemButton(Material.BEDROCK, inv);
		for (int i = inv.getSize()-7; i < inv.getSize(); i++) {
			inv.setItem(empty.clone(), i);
		}
	}

	public InteractionInventory getKitInventory() {
		return kit;
	}
	public InteractionInventory getArmorInventory() {
		return armor;
	}

	/**
	 * Gibt die Ausr�stung der Spieler zur�ck
	 * @return Die Ausr�stung der Spieler
	 */
	public ItemStack[][] getEquipment() {
		return equipment;
	}

	/**
	 * Stellt das Kit des Spiels neu ein
	 * @param kit Das neue Kit des Spiels
	 */
	public void setKit(ItemStack[] kit) {
		for (int i = 0; i < 36; i++) {
			equipment[0][i] = kit[i];
		}
		this.kit.setContents(kit);
		appendHotbar(this.kit, true);
		save();
	}
	/**
	 * Gibt das Kit des Spiels zur�ck
	 * @return Das Kit des Spiels
	 */
	public ItemStack[] getKit() {
		return equipment[0];
	}

	/**
	 * Stellt die R�stung des Spiels neu ein
	 * @param armor Die neue R�stung des Spiels
	 */
	public void setArmor(ItemStack[] armor) {
		for (int i = 0; i < 4; i++) {
			equipment[1][i] = armor[i];
		}
		this.armor.setContents(armor);
		appendHotbar(this.armor, false);
		save();
	}
	/**
	 * Gibt die R�stung des Events zur�ck
	 * @return Die R�stung des Events
	 */
	public ItemStack[] getArmor() {
		return equipment[1];
	}

	/**
	 * Speichert die Ausr�stung
	 */
	void save() {
		File file = warp.getCurrentEventFile();
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		CodeTools.saveInventory(equipment, cfg, path);
		try {
			cfg.save(file);
		} catch (IOException e) {
			errorHandler.ExceptionOccured(e);
		}
	}
	/**
	 * L�dt die gespeicherte Ausr�stung
	 * Ist keine Ausr�stung gespeichert, wird eine neue, leere Ausr�stung erstellt
	 */
	void load() {
		equipment = CodeTools.loadInventory(YamlConfiguration.loadConfiguration(warp.getCurrentEventFile()), path);

		kit.setContents(getKit());
		appendHotbar(kit, true);
		armor.setContents(getArmor());
		appendHotbar(armor, false);
	}

	@EventHandler
	public void onLobbyModeToggled(EventwarpToggleLobbymodeEvent e) {
		if (e.getEventwarp() == warp) {
			updatePath();
			load();
		}
	}

	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		try {
			String name = e.getView().getTitle();
			if (name == null) return;
			if (name.equals(EventEquipment.KitInvName)) {

				ItemStack[] kit = new ItemStack[36];
				ItemStack[] contents = e.getInventory().getContents();
				for (int i = 0; i < kit.length; i++) {
					kit[i] = contents[i];
				}
				setKit(kit);

			} else if (name.equals(EventEquipment.ArmorInvName)) {

				ItemStack[] armor = e.getInventory().getContents();
				ItemStack helmet = null;
				ItemStack chestplate = null;
				ItemStack leggings = null;
				ItemStack boots = null;

				//Als n�chstes muss das Zeug aussortiert werden, das nicht als R�stung infrage kommt
				for (int i = 8; i > -1; i--) {

					if (armor[i] != null) {
						Material type = armor[i].getType();
						if (isHelmet(type)) {
							helmet = armor[i];
						} else if (isChestplate(type)) {
							chestplate = armor[i];
						} else if (isLeggings(type)) {
							leggings = armor[i];
						} else if (isBoots(type)) {
							boots = armor[i];
						}
					}
				}

				//Und das, was in Frage kommt, wird gespeichert.
				ItemStack[] returnvalue = new ItemStack[4];
				returnvalue[0] = boots;
				returnvalue[1] = leggings;
				returnvalue[2] = chestplate;
				returnvalue[3] = helmet;

				setArmor(returnvalue);

			}


		} catch (Exception ex) {
			errorHandler.ListenerExceptionOccured(ex);
		}
	}

	private static boolean isHelmet(Material m) {
		return (m == Material.CHAINMAIL_HELMET
				|| m == Material.DIAMOND_HELMET
				|| m == Material.GOLDEN_HELMET
				|| m == Material.IRON_HELMET
				|| m == Material.LEATHER_HELMET
				|| m == Material.SKELETON_SKULL
				|| m == Material.WITHER_SKELETON_SKULL
				|| m == Material.PLAYER_HEAD
				|| m == Material.CREEPER_HEAD
				|| m == Material.ZOMBIE_HEAD
				|| m == Material.PUMPKIN);
	}
	private static boolean isChestplate(Material m) {
		return (m == Material.CHAINMAIL_CHESTPLATE
				|| m == Material.DIAMOND_CHESTPLATE
				|| m == Material.GOLDEN_CHESTPLATE
				|| m == Material.IRON_CHESTPLATE
				|| m == Material.LEATHER_CHESTPLATE);
	}
	private static boolean isLeggings(Material m) {
		return (m == Material.CHAINMAIL_LEGGINGS
				|| m == Material.DIAMOND_LEGGINGS
				|| m == Material.GOLDEN_LEGGINGS
				|| m == Material.IRON_LEGGINGS
				|| m == Material.LEATHER_LEGGINGS);
	}
	private static boolean isBoots(Material m) {
		return (m == Material.CHAINMAIL_BOOTS
				|| m == Material.DIAMOND_BOOTS
				|| m == Material.GOLDEN_BOOTS
				|| m == Material.IRON_BOOTS
				|| m == Material.LEATHER_BOOTS);
	}
}
