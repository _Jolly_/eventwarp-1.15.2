package de.jolly.eventwarp.controls;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.jolly.eventwarp.event.EventwarpCloseEvent;
import de.jolly.eventwarp.event.EventwarpOpenEvent;
import de.jolly.eventwarp.event.EventwarpPlayerChangeLivesEvent;
import de.jolly.eventwarp.event.EventwarpPlayerEnterSpectatormodeEvent;
import de.jolly.eventwarp.event.EventwarpPlayerLeaveSpectatormodeEvent;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.players.EventPlayer;

public class EventwarpLog implements Listener, EventwarpListener {

	private static final int MAX_LOG_SIZE = 50;

	private final Eventwarp warp;
	private final Set<CommandSender> listeners;
	private final List<String> log;

	public EventwarpLog(Eventwarp warp) {
		this.warp = warp;
		listeners = new HashSet<>();
		log = new ArrayList<String>();
	}

	/**
	 * Adds a new listener to this EventwarpLog
	 * Logged messages will be sent to this listener
	 */
	public void addListener(CommandSender listener) {
		listeners.add(listener);
	}
	/**
	 * Removes a listener from this EventwarpLog
	 * He will no longer receive messages from it.
	 */
	public void removeListener(CommandSender listener) {
		listeners.remove(listener);
	}
	/**
	 * Returns whether or not the given CommandSender
	 * is listening to this EventwarpLog
	 */
	public boolean isListening(CommandSender sender) {
		return listeners.contains(sender);
	}

	/**
	 * Shows the Log to the given CommandSender
	 */
	public void showLog(CommandSender sender) {
		StringBuilder message = new StringBuilder("�aLog:");
		for (String s: log) {
			message.append("\n�9"+s);
		}
		sendMessage(sender, message.toString());
	}
	/**
	 * Logs a message.
	 * @param message The message to be logged. If null, nothing will happen.
	 */
	public void log(String message) {
		if (message == null) {
			return;
		}
		addToLog(message);
		broadcastMessage(message);
	}

	private void addToLog(String message) {
		log.add(message);
		if (log.size() > MAX_LOG_SIZE) {
			log.remove(0);
		}
	}

	private void broadcastMessage(String message) {
		for (CommandSender s: listeners) {
			sendMessage(s, message);
		}
	}
	private void sendMessage(CommandSender sender, String message) {
		sender.sendMessage("�5[EW-Log]�9 "+message);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		listeners.remove(e.getPlayer());
	}

	/*
	 * I'd like to make a more generalized EventHandler instead of all the following ones
	 * which listens to the EventwarpEvent and all corresponding subclasses.
	 * 
	 * This is in the current state not possible, no listener which listens to the EventwarpEvent
	 * will be called because the EventwarpEvent has no HandlerList.
	 * The only way I'd see to make this possible is remove the HandlerLists from all Subclasses
	 * of EventwarpEvent and add one to the EventwarpEvent. This will however most likely
	 * have a negative effect on the performance of any called EventwarpEvent.
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEvent(EventwarpOpenEvent e) {
		if (e.getEventwarp() != warp) {
			return;
		}
		if (e.isCancelled()) {
			log("Opening of the Eventwarp was requested, but has been cancelled.");
		} else {
			log("The Eventwarp has been opened.");
		}
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEvent(EventwarpCloseEvent e) {
		if (e.getEventwarp() != warp) {
			return;
		}
		log("The Eventwarp has been closed.");
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEvent(EventwarpPlayerEnterSpectatormodeEvent e) {
		if (e.getEventwarp() != warp) {
			return;
		}
		log("Player entered Spectatormode: �3"+e.getPlayer());
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEvent(EventwarpPlayerLeaveSpectatormodeEvent e) {
		if (e.getEventwarp() != warp) {
			return;
		}
		log("Player left Spectatormode: �3"+e.getPlayer());
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEvent(EventwarpPlayerChangeLivesEvent e) {
		if (e.getEventwarp() != warp) {
			return;
		}
		if (e.isCancelled()) {
			log("�3"+e.getPlayer()+"'s�9 lives have been requested to be updated from �a"+(e.getPlayer().getLives())+"�9 to �a"+e.getResultingLives()+"�9, but the Event was cancelled.");
		} else {
			log("�3"+e.getPlayer()+"'s�9 lives have changed: From �a"+(e.getPlayer().getLives())+"�9 to �a"+e.getResultingLives());
		}
	}

	@Override
	public void onJoin(EventPlayer player) {
		log("Player joined: �3"+player.getName());
	}

	@Override
	public void onLeave(EventPlayer player) {
		log("Player left: �3"+player.getName());
	}

	@Override
	public void onRespawn(EventPlayer player) {
		log("Player respawned: �3"+player.getName());
	}

	@Override
	public void onDeath(EventPlayer player) {
		log("Player died: �3"+player.getName());
	}
}
