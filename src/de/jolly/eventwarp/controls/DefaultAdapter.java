package de.jolly.eventwarp.controls;

import org.bukkit.entity.Player;

import codetools.main.EnhancedJavaPlugin;
import de.jolly.eventwarp.external.EventwarpAdapter;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;

class DefaultAdapter implements EventwarpAdapter {
	
	private EventwarpMain main;
	
	DefaultAdapter(EventwarpMain main) {
		this.main = main;
	}

	@Override
	public void onJoin(EventPlayer player) {
	}

	@Override
	public void onLeave(EventPlayer player) {
	}

	@Override
	public void onRespawn(EventPlayer player) {
	}

	@Override
	public EventPlayer createEventPlayer(Player p, PlayerManager manager) {
		return null;
	}

	@Override
	public void onDetach() {
	}

	@Override
	public EnhancedJavaPlugin getMain() {
		return main;
	}

	@Override
	public void onAttach(Eventwarp warp) {
	}

	@Override
	public void onDeath(EventPlayer player) {
	}

	@Override
	public void onReload() {
	}

}
