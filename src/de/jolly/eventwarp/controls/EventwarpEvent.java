package de.jolly.eventwarp.controls;

import org.bukkit.event.Event;

public abstract class EventwarpEvent extends Event { 
	/**
	 * The Eventwarp-instance on which the Event was triggered.
	 */
	private Eventwarp warp;
	
	void setEventwarp(Eventwarp warp) {
		this.warp = warp;
	}
	/**
	 * Returns the Eventwarp-instance on which this Event was triggered
	 */
	public Eventwarp getEventwarp() {
		return warp;
	}
	
}
