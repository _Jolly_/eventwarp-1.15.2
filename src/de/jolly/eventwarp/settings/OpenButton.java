package de.jolly.eventwarp.settings;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;

/**
 * This Button implies whether or not the Eventwarp is currently open
 * and allows to change that state.
 * @author _Jolly_
 *
 */
public class OpenButton extends ItemButton {
	
	private final Eventwarp warp;
	
	public OpenButton(Eventwarp warp, InteractionInventory inv) {
		super(Material.RED_WOOL, null, inv);
		this.warp = warp;
		setTask(task);
	}
	
	/**
	 * Updates the button's meta based on whether the
	 * Eventwarp is open or not.
	 */
	void updateMetaForState(boolean open) {
		ItemMeta meta = getItemMeta();
		if (open) {
			meta.setDisplayName("�aDer Eventwarp ist offen.");
			setType(Material.LIME_WOOL);
		} else {
			meta.setDisplayName("�cDer Eventwarp ist geschlossen.");
			setType(Material.RED_WOOL);
		}
		setItemMeta(meta);
	}
	
	private ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			warp.setOpen(!warp.isOpen());
			updateMetaForState(warp.isOpen());
			warp.getSettings().getControlPanel().update();
		}
	};
	
}
