package de.jolly.eventwarp.settings;

import javax.annotation.OverridingMethodsMustInvokeSuper;

import org.bukkit.configuration.file.FileConfiguration;

import de.jolly.eventwarp.settings.tangible.BroadcastSettingUpdatesSetting;

public abstract class Setting {
	
	/**
	 * The name of this setting
	 */
	private final String name;
	private final EventwarpSettings settings;
	
	public Setting(String name, EventwarpSettings settings) {
		this.name = name;
		this.settings = settings;
	}
	
	/**
	 * Writes the current state of this option into the FileConfiguration
	 * @param path The path where to save the data. This rather points to the place
	 * where generally all settings are storaged, as the name of this setting will
	 * automatically be appended to the path.
	 */
	abstract void writeToFile(FileConfiguration cfg, String path);
	/**
	 * Changes the Setting from the data storaged in the FileConfiguration
	 * @param path The path where to save the data. This rather points to the place
	 * where generally all settings are storaged, as the name of this setting will
	 * automatically be appended to the path.
	 */
	abstract void loadFromFile(FileConfiguration cfg, String path);
	
	public String getName() {
		return name;
	}
	
	protected EventwarpSettings getSettings() {
		return settings;
	}
	
	/**
	 * Adds an ItemStack which represents this setting into the Panel
	 * Usually the Setting will add an ItemButton which can be used to edit
	 * the setting.
	 */
	public abstract void addToPanel(ControlPanel panel);
	
	//Messages
	/**
	 * Called when the state of this Setting changes.
	 * Please invoke the method of the superclass when
	 * overriding this method.
	 * This method is only being called when the setting has been changed, not
	 * when it has been loaded from the file!
	 */
	@OverridingMethodsMustInvokeSuper
	protected void onUpdate() {
		Setting s = settings.getSetting(BroadcastSettingUpdatesSetting.NAME);
		if (s != null) {
			((BroadcastSettingUpdatesSetting)s).onSettingUpdated(this);
		}
	}
	/**
	 * Called when the state of the Setting has been loaded from the file.
	 */
	protected void onLoad() {
		
	}
}
