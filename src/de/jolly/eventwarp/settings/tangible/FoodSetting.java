package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class FoodSetting extends BooleanSettingButton {
	
	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "fillfood";
	private static final int DELAY = 100;
	
	private int task;
	private final PlayerManager manager;
	private final Plugin main;
		
	public FoodSetting(PlayerManager manager, Plugin main, EventwarpSettings settings) {
		super(NAME, settings);
		this.manager = manager;
		this.main = main;
		task = -1;
	}

	@Override
	protected int getSlot() {
		return 32;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.COOKED_BEEF);
		} else {
			button.setType(Material.PORKCHOP);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		if (isEnabled()) {
			List<String> lore = new ArrayList<>();
			lore.add("Der Hunger wird alle 5 Sekunden aufgef�llt");
			lore.add("Das Auff�llen wird auch dann regelm��ig ausgef�hrt,");
			lore.add("wenn kein Spieler im Event ist, daher sollte diese");
			lore.add("Einstellung nur aktiviert werden, wenn sie gebraucht wird.");
			meta.setLore(lore);
		} else {
			meta.setLore(new ArrayList<String>());
		}
		button.setItemMeta(meta);
	}
	
	private final BukkitRunnable fillTask = new BukkitRunnable() {
		@Override
		public void run() {
			for (EventPlayer all: manager.getAllPlayers()) {
				all.getPlayer().setFoodLevel(20);
			}
		}
	};
	
	@Override
	public void setEnabled(boolean value) {
		super.setEnabled(value);
		if (isEnabled()) {
			startTask();
		} else {
			cancelTask();
		}
	}
	@Override
	protected void onLoad() {
		cancelTask();
		if (isEnabled()) {
			startTask();
		}
	}
	
	@SuppressWarnings("deprecation")
	private void startTask() {
		if (isActive()) return;
		task = Bukkit.getScheduler().scheduleAsyncRepeatingTask(main, fillTask, 0, DELAY);
		
	}
	private void cancelTask() {
		if (!isActive()) return;
		Bukkit.getScheduler().cancelTask(task);
		task = -1;
	}
	private boolean isActive() {
		return task != -1;
	}

	@Override
	public String toString() {
		if (isEnabled()) {
			return "�9Immer voller Hunger: �aAktiviert";
		} else {
			return "�9Immer voller Hunger: �cDeaktiviert";
		}
	}
}
