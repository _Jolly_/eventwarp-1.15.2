package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class InvincibilitySetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "invincibility";
	
	private Eventwarp warp;

	public InvincibilitySetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 15;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.DIAMOND_CHESTPLATE);
		} else {
			button.setType(Material.LEATHER_CHESTPLATE);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		if (isEnabled()) {
			meta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 10, true);
		} else {
			meta.removeEnchant(Enchantment.PROTECTION_ENVIRONMENTAL);
		}
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		button.setItemMeta(meta);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (isEnabled()) {
			if (!(e.getEntity() instanceof Player)) {
				return;
			}
			if (warp.getPlayerManager().getPlayer((Player)e.getEntity()) != null) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "§9Unverwundbarkeit: §aAktiviert";
		} else {
			return "§9Unverwundbarkeit: §cDeaktiviert";
		}
	}

}
