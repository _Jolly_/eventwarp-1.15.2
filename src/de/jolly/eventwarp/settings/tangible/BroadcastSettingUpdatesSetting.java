package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;
import de.jolly.eventwarp.settings.Setting;

public class BroadcastSettingUpdatesSetting extends BooleanSettingButton {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "broadcastupdates";
	
	private final Eventwarp warp;
	
	public BroadcastSettingUpdatesSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 8;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value) {
			button.setType(Material.WRITTEN_BOOK);
		} else {
			button.setType(Material.BOOK);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		
		button.setItemMeta(meta);
	}
	
	public void onSettingUpdated(Setting s) {
		if (!value || s == this) {
			return;
		}
		for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
			all.getPlayer().sendMessage("�b[Eventwarp-Settings] �9"+s.toString());
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Einstellungs-Updatenachrichten: �aAktiviert";
		} else {
			return "�9Einstellungs-Updatenachrichten: �cDeaktiviert";
		}
	}

}
