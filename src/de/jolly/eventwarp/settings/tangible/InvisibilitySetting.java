package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.event.EventwarpPlayerLeaveSpectatormodeEvent;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class InvisibilitySetting extends BooleanSettingButton implements Listener, EventwarpListener {
	
	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "invisibility";
	
	private Eventwarp warp;
	private final Plugin main;
	
	public InvisibilitySetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
		main = warp.getEventwarpMain();
	}
	
	@Override
	public void updateItem(ItemButton potion) {
		if (isEnabled()) {
			potion.setType(Material.POTION);
			PotionMeta meta = (PotionMeta) potion.getItemMeta();
			PotionData data = new PotionData(PotionType.INVISIBILITY);
			meta.setBasePotionData(data);
			potion.setItemMeta(meta);
			
		} else {
			potion.setType(Material.GLASS_BOTTLE);
		}
		
		ItemMeta meta = potion.getItemMeta();
		meta.setDisplayName(toString());
		List<String> lore = new ArrayList<>();
		lore.add("Die Unsichtbarkeit gilt nur");
		lore.add("gegen�ber anderen Eventteilnehmern!");
		meta.setLore(lore);
		potion.setItemMeta(meta);
		
	}

	@Override
	public void setEnabled(boolean value) {
		if (isEnabled() != value) {
			EventPlayer[] list = warp.getPlayerManager().getAllPlayers();
			for (EventPlayer p: list) {
				Player pl = p.getPlayer();
				for (EventPlayer a: list) {
					if (value) {
						pl.hidePlayer(main, a.getPlayer());
					} else {
						pl.showPlayer(main, a.getPlayer());
					}
				}
			}
			warp.getPlayerManager().getSpectatorManager().reloadInvisibility();
		}

		super.setEnabled(value);
	}
	
	@Override
	public void onJoin(EventPlayer player) {
		if (!isEnabled()) return;
		
		Player p = player.getPlayer();
		
		for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
			if (!all.isSpectatormode()) {
				all.getPlayer().hidePlayer(main, p);
			}
		}
		
		for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
			p.hidePlayer(main, all.getPlayer());
		}
		
	}

	@Override
	public void onLeave(EventPlayer player) {
		Player p = player.getPlayer();
		for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
			p.showPlayer(main, all.getPlayer());
			all.getPlayer().showPlayer(main, p);
		}
	}

	@Override
	public void onRespawn(EventPlayer player) {}

	@Override
	protected int getSlot() {
		return 14;
	}

	@Override
	public void onDeath(EventPlayer player) {
	}
	
	@EventHandler
	public void onDisableSpectatormode(EventwarpPlayerLeaveSpectatormodeEvent e) {
		if (e.getEventwarp() == warp && isEnabled()) {
			for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
				e.getPlayer().getPlayer().hidePlayer(main, all.getPlayer());
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Unsichtbarkeit: �aAktiviert";
		} else {
			return "�9Unsichtbarkeit: �cDeaktiviert";
		}
	}
}
