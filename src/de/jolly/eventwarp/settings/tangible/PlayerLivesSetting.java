package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.settings.EventwarpSettings;
import de.jolly.eventwarp.settings.IntegerSettingButton;

public class PlayerLivesSetting extends IntegerSettingButton implements EventwarpListener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "lives";
	
	private final Eventwarp warp;
	private final Set<EventPlayer> enableSpectator;
	
	public PlayerLivesSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings, 0, 50);
		this.warp = warp;
		enableSpectator = new HashSet<>();
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value == 0) {
			button.setType(Material.RED_DYE);
			button.addUnsafeEnchantment(org.bukkit.enchantments.Enchantment.PROTECTION_ENVIRONMENTAL, 10);
		} else {
			button.setType(Material.POTION);
			button.setDurability((short)8197);
			button.setAmount(getValue());
			button.removeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL);
			ItemMeta meta = button.getItemMeta();
			List<String> lore = new ArrayList<>();
			lore.add("Leben werden nur bei geschlossenem");
			lore.add("Eventwarp abgezogen.");
			lore.add("Diesen Wert zu �ndern hat");
			lore.add("keine Auswirkung auf die aktuellen");
			lore.add("Leben der Spieler oder ob sie sich");
			lore.add("im Moment im Spectatormode befinden.");
			meta.setLore(lore);
			button.setItemMeta(meta);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	
	@Override
	protected int getSlot() {
		return 5;
	}

	@Override
	public void onJoin(EventPlayer player) {
	}

	@Override
	public void onLeave(EventPlayer player) {
	}

	@Override
	public void onRespawn(final EventPlayer player) {
		if (doRulesApply(player) && enableSpectator.contains(player)) {
			enableSpectator.remove(player);
			//Enabling flight on players who just respawned doesn't work properly, that's why I included that delay
			Bukkit.getScheduler().scheduleSyncDelayedTask(warp.getEventwarpMain(), new Runnable() {
				@Override
				public void run() {
					player.enableSpectatormode();
				}
			}, 5);
		}
	}

	@Override
	public void onDeath(EventPlayer player) {
		if (doRulesApply(player)) {
			if (player.getLives() == 1) {
				enableSpectator.add(player);
			}
			
			player.setLives(player.getLives()-1);
		}
	}
	
	/**
	 * Checkes whether or not the rules given by this setting
	 * currently apply to the given player.
	 * Just look at the source code and you'll probably understand
	 * what I mean.
	 */
	private boolean doRulesApply(EventPlayer player) {
		if (player.isSpectatormode()) {
			return false;
		}
		if (value == 0) {
			return false;
		}
		if (warp.isOpen()) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		if (value == 0) {
			return "Anzahl an Leben: Unendlich";
		} else {
			return "Anzahl an Leben: "+getValue();
		}
	}
}
