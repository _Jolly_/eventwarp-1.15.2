package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class BuildSetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "build";
	
	private final Eventwarp warp;
	
	public BuildSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 24;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value) {
			button.setType(Material.DIAMOND_PICKAXE);
		} else {
			button.setType(Material.STICK);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		
		button.setItemMeta(meta);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBuild(BlockPlaceEvent e) {
		if (!isEnabled()) {
			if (warp.getPlayerManager().getPlayer(e.getPlayer()) != null) {
				e.setCancelled(true);
			}
		}
	}
	@EventHandler(priority = EventPriority.HIGH)
	public void onBreak(BlockBreakEvent e) {
		if (!isEnabled()) {
			if (warp.getPlayerManager().getPlayer(e.getPlayer()) != null) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Bauen: �aErlaubt";
		} else {
			return "�9Bauen: �cVerboten";
		}
	}
	
}
