package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import codetools.main.CodeTools;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class PvPSetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "pvp";
	
	private final Eventwarp warp;
	
	public PvPSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}
	
	@Override
	protected int getSlot() {
		return 23;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.DIAMOND_SWORD);
		} else {
			button.setType(Material.STICK);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	@EventHandler
	public void onPvP(EntityDamageByEntityEvent e) {
		if (!isEnabled()) {
			if (!(e.getEntity() instanceof Player)) {
				return;
			}
			Player attacker = CodeTools.getPlayerDamager(e);
			if (attacker == null) {
				return;
			}
			Player victim = (Player) e.getEntity();
			if (warp.getPlayerManager().getPlayer(attacker) != null
					&& warp.getPlayerManager().getPlayer(victim) != null) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "§9PvP: §aAktiviert";
		} else {
			return "§9PvP: §cDeaktiviert";
		}
	}
}
