package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import codetools.main.CodeTools;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.event.EventwarpPlayerSwitchTeamEvent;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.Team;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class TeamColoredArmorSetting extends BooleanSettingButton implements Listener, EventwarpListener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "teamcolorarmor";
	
	private final Eventwarp warp;
	
	public TeamColoredArmorSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}
	
	@Override
	protected int getSlot() {
		return 35;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.CYAN_WOOL);
		} else {
			button.setType(Material.WHITE_WOOL);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	@Override
	protected void initializeItem(ItemButton button) {
		button.setType(Material.WHITE_WOOL);
		ItemMeta meta = button.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		lore.add("Ist diese Einstellung aktiv, wird Lederr�stung von Spielern in der Farbe");
		lore.add("ihres Teams eingef�rbt, sofern ein verbundenes Plugin");
		lore.add("keine eigenen �nderungen an der Farbe der R�stung vornimmt.");
		button.setItemMeta(meta);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onPvP(EntityDamageByEntityEvent e) {
		if (!isEnabled() && !e.isCancelled()) {
			if (!(e.getEntity() instanceof Player)) {
				return;
			}
			Player a = CodeTools.getPlayerDamager(e);
			if (a == null) return;
			EventPlayer attacker = warp.getPlayerManager().getPlayer(a);
			if (attacker == null) {
				return;
			}
			EventPlayer victim = warp.getPlayerManager().getPlayer((Player) e.getEntity());
			
			if (victim != null && attacker.getTeam() != null && victim.getTeam() == attacker.getTeam()) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Teamfarben auf R�stung: �aAktiviert";
		} else {
			return "�9Teamfarben auf R�stung: �cDeaktiviert";
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		for (EventPlayer p: warp.getPlayerManager().getAllPlayers()) {
			if (enabled) {
				apply(p);
			} else {
				p.setClothesColor(null);
			}
		}
	}
	
	@EventHandler
	public void onTeamSwitch(EventwarpPlayerSwitchTeamEvent e) {
		if (e.getEventwarp() == warp) {
			apply(e.getPlayer());
		}
	}
	
	@Override
	public void onJoin(EventPlayer player) {
	}

	@Override
	public void onLeave(EventPlayer player) {
	}

	@Override
	public void onRespawn(EventPlayer player) {
		apply(player);
	}

	@Override
	public void onDeath(EventPlayer player) {
	}
	
	private void apply(EventPlayer p) {
		Team t = p.getTeam();
		if (t != null) {
			p.setClothesColor(t.toColor());
		}
	}
}
