package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class FallDamageSetting extends BooleanSettingButton {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "falldmg";
	private Eventwarp warp;

	public FallDamageSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;	
	}

	@Override
	protected int getSlot() {
		return 16;
	}
	
	@Override
	protected void updateItem(ItemButton button) {
		button.setType(Material.FEATHER);
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		if (isEnabled()) {
			meta.addEnchant(Enchantment.PROTECTION_FALL, 10, true);
		} else {
			meta.removeEnchant(Enchantment.PROTECTION_FALL);
		}
		button.setItemMeta(meta);
	}

	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (isEnabled()) {
			if (!(e.getEntity() instanceof Player)) {
				return;
			}
			if (warp.getPlayerManager().getPlayer((Player)e.getEntity()) != null) {
				if (e.getCause() == DamageCause.FALL) {
					e.setCancelled(true);
				}
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "§9Fallschaden: §aBlockiert";
		} else {
			return "§9Fallschaden: §cAktiviert";
		}
	}
}
