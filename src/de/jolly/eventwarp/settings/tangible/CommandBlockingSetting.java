package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

/**
 * This class represents a setting which allows you to block commands at the Eventwarp.
 * If this setting is enabled, commands at the Eventwarp are being blocked.
 * THIS CLASS NEEDS TO BE REWORKED IF THE PLUGIN SHALL BE REBUILT FOR HAVING MULTIPLE EVENTWARP-INSTANCES
 * @author _Jolly_
 *
 */
public class CommandBlockingSetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "commandblocking";
	
	private static final String BYPASS_PERMISSION = "eventwarp.commandblocking";
	private final EventwarpMain main;
	private final PlayerManager manager;
	
	public CommandBlockingSetting(PlayerManager manager, EventwarpMain main, EventwarpSettings settings) {
		super(NAME, settings);
		this.main = main;
		this.manager = manager;
	}
	
	
	
	protected void updateItem(ItemButton item) {
		if (isEnabled()){
			item.setType(Material.REDSTONE_BLOCK);
		} else {
			item.setType(Material.COMMAND_BLOCK);
		}
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(toString());
		if (isEnabled()) {
			List<String> lore = new ArrayList<>();
			lore.add("Spieler mit der Permission �9"+BYPASS_PERMISSION);
			lore.add("k�nnen trotzdem andere Commands benutzen.");
			meta.setLore(lore);
		} else {
			meta.setLore(null);
		}
		item.setItemMeta(meta);
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (!isEnabled()) {
			return;
		}
		
		if (manager.getPlayer(e.getPlayer()) == null) {
			return;
		}
		
		String command = main.command().toLowerCase();
		String evCommand = main.getEventCommand().toLowerCase();
		
		if (e.getMessage() == null || e.getMessage().length() < 2) {
			return;
		}
		
		String msg = e.getMessage().substring(1).toLowerCase();
		if (msg.equals(command) || msg.equals(evCommand)) {
			return;
		}
		if (msg.startsWith(command+" ") || msg.startsWith(evCommand+" ")) {
			return;
		}
		if (e.getPlayer().hasPermission(BYPASS_PERMISSION)) {
			return;
		}
		
		main.debug.send("Command wird blockiert: �c"+e.getMessage()+"�5 gesendet von �3"+e.getPlayer().getName());
		main.sendMessage(e.getPlayer(), "Du kannst hier die meisten Befehle nicht nutzen. �6/"+main.getEventCommand()+" leave�defM zum Verlassen des Events.");
		e.setCancelled(true);
		e.setMessage("/"+codetools.main.APIMain.emptyCommand);
	}

	@Override
	protected int getSlot() {
		return 25;
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�cEventwarp-fremde Commands werden blockiert.";
		} else {
			return "�aEventwarp-fremde Commands werden zugelassen.";
		}
	}
}
