package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class DropItemsSetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "dropitems";
	
	private final Eventwarp warp;
	
	public DropItemsSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 26;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value) {
			button.setType(Material.DEAD_BUSH);
		} else {
			button.setType(Material.OAK_SAPLING);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onItemDrop(PlayerDropItemEvent e) {
		if (!isEnabled()) {
			if (warp.getPlayerManager().getPlayer(e.getPlayer()) != null) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Droppen von Items: �aErlaubt";
		} else {
			return "�9Droppen von Items: �cVerboten";
		}
	}

}
