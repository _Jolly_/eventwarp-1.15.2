package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class LoseItemsSetting extends BooleanSettingButton implements Listener {
	
	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "loseitems";
	
	private final Eventwarp warp;
	
	public LoseItemsSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 17;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value) {
			button.setType(Material.CHEST);
		} else {
			button.setType(Material.ENDER_CHEST);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e) {
		if (!isEnabled()) {
			if (warp.getPlayerManager().getPlayer(e.getEntity()) != null) {
				e.getDrops().clear();
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "§9Itemdrop bei Tod: §aAktiviert";
		} else {
			return "§9Itemdrop bei Tod: §cDeaktiviert";
		}
	}
}
