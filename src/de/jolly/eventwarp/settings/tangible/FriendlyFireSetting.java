package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import codetools.main.CodeTools;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class FriendlyFireSetting extends BooleanSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "friendlyfire";

	private final Eventwarp warp;

	public FriendlyFireSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 34;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.GOLDEN_SWORD);
		} else {
			button.setType(Material.STICK);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}

	@Override
	protected void initializeItem(ItemButton button) {
		ItemMeta meta = button.getItemMeta();
		ArrayList<String> lore = new ArrayList<>();
		lore.add("Friendly Fire erlaubt, dass sich Mitglieder des gleichen Teams");
		lore.add("gegenseitig angreifen.");
		button.setItemMeta(meta);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPvP(EntityDamageByEntityEvent e) {
		if (!isEnabled() && !e.isCancelled()) {
			if (!(e.getEntity() instanceof Player)) {
				return;
			}
			Player a = CodeTools.getPlayerDamager(e);
			if (a == null) return;
			EventPlayer attacker = warp.getPlayerManager().getPlayer(a);
			if (attacker == null) {
				return;
			}
			EventPlayer victim = warp.getPlayerManager().getPlayer((Player) e.getEntity());

			if (victim != null && attacker.getTeam() != null && victim.getTeam() == attacker.getTeam()) {
				e.setCancelled(true);
			}
		}
	}

	@Override
	public String toString() {
		if (value) {
			return "§9Friendly Fire: §aAktiviert";
		} else {
			return "§9Friendly Fire: §cDeaktiviert";
		}
	}

}
