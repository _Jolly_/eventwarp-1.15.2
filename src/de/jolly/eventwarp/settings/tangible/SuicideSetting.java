package de.jolly.eventwarp.settings.tangible;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

/**
 * This Setting determines whether or not the
 * players are allowed to suicide at the Eventwarp.
 * It does not directly control the suicide command, as making this class
 * both a Setting and a CommandExecutor would have been complicated in terms
 * of properly registering.
 * @author _Jolly_
 *
 */
public class SuicideSetting extends BooleanSettingButton {
	
	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "suicide";
	
	public SuicideSetting(EventwarpSettings settings) {
		super(NAME, settings);
	}

	@Override
	protected int getSlot() {
		return 7;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (value) {
			button.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 10);
		} else {
			button.removeEnchantment(Enchantment.ARROW_DAMAGE);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	@Override
	protected void initializeItem(ItemButton button) {
		button.setType(Material.SKELETON_SKULL);
		ItemMeta meta = button.getItemMeta();
		List<String> lore = new ArrayList<>();
		lore.add("Diese Einstellung erlaubt oder verbietet");
		lore.add("Die Nutzung des Selbstmord-Befehls.");
		meta.setLore(lore);
		button.setItemMeta(meta);
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Selbstmord: �aErlaubt";
		} else {
			return "�9Selbstmord: �cVerboten";
		}
	}
}
