package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.BooleanSettingButton;
import de.jolly.eventwarp.settings.EventwarpSettings;

public class BlockItemMoveSetting extends BooleanSettingButton implements Listener {
	
	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "moveitems";
	
	private final PlayerManager manager;
	
	public BlockItemMoveSetting(PlayerManager manager, EventwarpSettings settings) {
		super(NAME, settings);
		this.manager = manager;
	}

	@Override
	protected int getSlot() {
		return 33;
	}

	@Override
	protected void updateItem(ItemButton button) {
		if (isEnabled()) {
			button.setType(Material.STICKY_PISTON);
		} else {
			button.setType(Material.PISTON);
		}
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onItemInteract(InventoryClickEvent e) {
		if (isEnabled()) {
			if (manager.getPlayer((Player)e.getWhoClicked()) != null) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public String toString() {
		if (value) {
			return "�9Bewegen von Items im Inventar: �cVerboten";
		} else {
			return "�9Bewegen von Items im Inventar: �aErlaubt";
		}
	}

}
