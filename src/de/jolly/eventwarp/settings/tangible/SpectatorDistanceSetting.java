package de.jolly.eventwarp.settings.tangible;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.settings.ControlPanel;
import de.jolly.eventwarp.settings.EventwarpSettings;
import de.jolly.eventwarp.settings.GeneralIntegerSettingButton;

public class SpectatorDistanceSetting extends GeneralIntegerSettingButton implements Listener {

	/**
	 * The static name of this Setting.
	 * You can use this to retrieve the instance of this
	 * Setting from the EventwarpSettings-instance.
	 */
	public static final String NAME = "spectatordistance";
	
	private final Eventwarp warp;
	
	public SpectatorDistanceSetting(Eventwarp warp, EventwarpSettings settings) {
		super(NAME, settings, 0, 10);
		this.warp = warp;
	}

	@Override
	protected int getSlot() {
		return 6;
	}

	@Override
	protected void updateItem(ItemButton button) {
		super.updateItem(button);
		if (value == 0) {
			button.removeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL);
			button.setAmount(1);
		} else {
			button.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 10);
		}
	}

	@Override
	protected ItemButton createItem(ControlPanel panel) {
		return new ItemButton(Material.CACTUS, panel);
	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e) {
		if (value == 0) {
			return;
		}
		
		try {
			
			EventPlayer p = warp.getPlayerManager().getPlayer(e.getPlayer());
			if (p == null) {
				return;
			}
			int distance = getValue();
			if (!p.isSpectatormode()) {

				Player p2 = p.getPlayer();
				for (Entity entity: p2.getNearbyEntities(distance, distance, distance)) {
					if (entity instanceof Player) {
						Player p1 = (Player) entity;
						EventPlayer ep1 = warp.getPlayerManager().getPlayer(p1);
						if (ep1 != null && ep1.isSpectatormode()) {
							pushPlayer(p1, p2);
						}
					}
				}

			} else  {

				Player p1 = p.getPlayer();
				for (Entity entity: p1.getNearbyEntities(distance, distance, distance)) {
					if (entity instanceof Player) {
						Player p2 = (Player) entity;
						EventPlayer ep2 = warp.getPlayerManager().getPlayer(p2);
						if (ep2 != null && !ep2.isSpectatormode()) {
							pushPlayer(p1, p2);
						}
					}
				}

			}
		} catch (Exception ex) {
			warp.getEventwarpMain().getErrorHandler().ListenerExceptionOccured(ex);
		}
	}

	/**
	 * @param p1 Der Spieler, der weggesto�en werden soll
	 * @param p2 Der Spieler, von dem der andere weggesto�en werden soll
	 */
	private void pushPlayer(Player p1, Player p2) {

		double ax = p1.getLocation().getX();
		double ay = p1.getLocation().getY();
		double az = p1.getLocation().getZ();

		double bx = p2.getLocation().getX();
		double by = p2.getLocation().getY();
		double bz = p2.getLocation().getZ();

		double x = ax - bx;
		double y = ay - by;
		double z = az - bz;

		Vector v = new Vector(x, y, z).normalize().multiply(1.5D).setY(0.2D);
		p1.setVelocity(v);
	}
	

	@Override
	public String toString() {
		if (value == 0) {
			return "�cSpectator k�nnen sich Spielern n�hern.";
		} else {
			return "�9Mindestabstand Spectator-Spieler: �a"+getValue();
		}
	}

}
