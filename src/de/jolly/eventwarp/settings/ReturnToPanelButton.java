package de.jolly.eventwarp.settings;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;

public class ReturnToPanelButton extends ItemButton {
	
	public ReturnToPanelButton(final InteractionInventory inv, final ControlPanel panel) {
		super(Material.BLUE_WOOL, "�bZur�ck zum ControlPanel", new ButtonTask() {
			@Override
			public void execute(ItemButton button, InventoryClickEvent e) {
				e.getWhoClicked().closeInventory();
				panel.showToPlayer((Player)e.getWhoClicked());
			}
		}, inv);
	}
	
}
