package de.jolly.eventwarp.settings;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.event.EventwarpToggleLobbymodeEvent;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.settings.tangible.BlockItemMoveSetting;
import de.jolly.eventwarp.settings.tangible.BroadcastSettingUpdatesSetting;
import de.jolly.eventwarp.settings.tangible.BuildSetting;
import de.jolly.eventwarp.settings.tangible.CommandBlockingSetting;
import de.jolly.eventwarp.settings.tangible.DropItemsSetting;
import de.jolly.eventwarp.settings.tangible.FallDamageSetting;
import de.jolly.eventwarp.settings.tangible.FoodSetting;
import de.jolly.eventwarp.settings.tangible.FriendlyFireSetting;
import de.jolly.eventwarp.settings.tangible.InvincibilitySetting;
import de.jolly.eventwarp.settings.tangible.InvisibilitySetting;
import de.jolly.eventwarp.settings.tangible.LoseItemsSetting;
import de.jolly.eventwarp.settings.tangible.PlayerLivesSetting;
import de.jolly.eventwarp.settings.tangible.PvPSetting;
import de.jolly.eventwarp.settings.tangible.SpectatorDistanceSetting;
import de.jolly.eventwarp.settings.tangible.SuicideSetting;
import de.jolly.eventwarp.settings.tangible.TeamColoredArmorSetting;

public class EventwarpSettings implements Listener {
	
	private final String DEFAULT_PATH = "settings";
	private final String LOBBYMODE_PATH = "lobbysettings";
	private String path;
	
	private final Eventwarp warp;
	private final ControlPanel panel;
	private final EventwarpMain main;
	
	private final Set<Setting> settings;
	
	public EventwarpSettings(Eventwarp warp, EventwarpMain main) {
		this.warp = warp;
		this.main = main;
		settings = new HashSet<>();
		panel = new ControlPanel(main, warp);
		main.getServer().getPluginManager().registerEvents(this, main);
		main.getServer().getPluginManager().registerEvents(panel, main);
		updatePath();
	}
	
	private void updatePath() {
		if (warp.isLobbyMode()) {
			path = LOBBYMODE_PATH;
		} else {
			path = DEFAULT_PATH;
		}
	}
	
	/**
	 * Returns a Setting instance from its name.
	 * @param name The name of the Setting (case sensitive!)
	 * @return The instance of the Setting with the given name or null,
	 * if no such Setting exists.
	 */
	public Setting getSetting(String name) {
		for (Setting s: settings) {
			if (s.getName().equals(name)) {
				return s;
			}
		}
		return null;
	}
	
	/**
	 * Removes all loaded settings and fully reloads the EventwarpSettings.
	 * This includes:
	 *   - Clearing the ControlPanel
	 *   - Recreating the EventwarpSettings
	 *   - Reloading the additional Buttons on the ControlPanel
	 *   - Loading the state of the Settings
	 *   - Reloading and Updating the ControlPanel
	 *   - Registering the Setting's Listeners
	 * This can be useful to remove EventwarpSettings which were
	 * added by external plugins when you're detaching them again.
	 */
	public void reloadAll() {
		for (Setting s: settings) {
			if (s instanceof EventwarpListener) {
				warp.unregisterListener((EventwarpListener)s);
			}
			if (s instanceof Listener) {
				HandlerList.unregisterAll((Listener)s);
			}
		}
		settings.clear();
		
		warp.unregisterListener(panel);//Just to make sure the panel won't be registered twice.
		warp.registerListener(panel);
		
		createSettings();
		load();
		reloadPanel();
	}
	
	private void createSettings() {
		addSettingWithoutReload(new BroadcastSettingUpdatesSetting(warp, this));
		addSettingWithoutReload(new InvisibilitySetting(warp, this));
		addSettingWithoutReload(new CommandBlockingSetting(warp.getPlayerManager(), main, this));
		addSettingWithoutReload(new PvPSetting(warp, this));
		addSettingWithoutReload(new InvincibilitySetting(warp, this));
		addSettingWithoutReload(new FallDamageSetting(warp, this));
		addSettingWithoutReload(new PlayerLivesSetting(warp, this));
		addSettingWithoutReload(new SpectatorDistanceSetting(warp, this));
		addSettingWithoutReload(new SuicideSetting(this));
		addSettingWithoutReload(new LoseItemsSetting(warp, this));
		addSettingWithoutReload(new DropItemsSetting(warp, this));
		addSettingWithoutReload(new BuildSetting(warp, this));
		addSettingWithoutReload(new FoodSetting(warp.getPlayerManager(), main, this));
		addSettingWithoutReload(new BlockItemMoveSetting(warp.getPlayerManager(), this));
		addSettingWithoutReload(new FriendlyFireSetting(warp, this));
		addSettingWithoutReload(new TeamColoredArmorSetting(warp, this));
	}
	
	/**
	 * This adds a new Setting to the EventwarpSettings.
	 * If the Setting is a Listener or an EventwarpListener it will be registered at the
	 * corresponding classes.
	 * This method does neither reload the ControlPanel, causing the new Items to not appear within it,
	 * nor does it load the state of the Settings from the File.
	 * @param s The Setting to be added.
	 */
	public void addSettingWithoutReload(Setting s) {
		if (settings.contains(s)) {
			return;
		}
		
		settings.add(s);
		

		if (s instanceof EventwarpListener) {
			warp.registerListener((EventwarpListener) s);
		}
		
		if (s instanceof Listener) {
			main.getServer().getPluginManager().registerEvents((Listener)s, warp.getAdapter().getMain());
		}
	}
	/**
	 * This adds a new Setting to the EventwarpSettings.
	 * If the Setting is a Listener or an EventwarpListener it will be registered at the
	 * corresponding classes.
	 * This method reloads the ControlPanel, causing the new Items to appear within it,
	 * ad it loads the state of the Settings from the File.
	 * 
	 * (Ima be honest with you, this method just calls addSettingWithoutReload(), load() and reloadPanel())
	 * 
	 * @param s The Setting to be added.
	 */
	public void addSetting(Setting s) {
		addSettingWithoutReload(s);
		
		load();
		reloadPanel();
	}
	/**
	 * Reloads the ControlPanel by deleting all Items inside and recreating them.
	 */
	public void reloadPanel() {
		panel.setContents(null);
		panel.createAdditionalButtons(main, warp);
		for (Setting s: settings) {
			s.addToPanel(panel);
		}
		panel.update();
	}
	
	/**
	 * Loads the state of the registered Settings from the eventwarp.yml file
	 */
	public void load() {
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(warp.getCurrentEventFile());
		for (Setting s: settings) {
			s.loadFromFile(cfg, path);
		}
		reloadPanel();
		
	}
	public void save() {
		File f = warp.getCurrentEventFile();
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(f);
		for (Setting s: settings) {
			s.writeToFile(cfg, path);
		}
		
		try {
			cfg.save(f);
		} catch (IOException e) {
			main.getErrorHandler().ExceptionOccured(e);
		}
	}
	
	public ControlPanel getControlPanel() {
		return panel;
	}
	
	@EventHandler
	public void onLobbyModeToggled(EventwarpToggleLobbymodeEvent e) {
		if (e.getEventwarp() == warp) {
			updatePath();
			load();
		}
	}
}