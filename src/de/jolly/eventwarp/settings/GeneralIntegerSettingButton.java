package de.jolly.eventwarp.settings;

import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ItemButton;

public abstract class GeneralIntegerSettingButton extends IntegerSettingButton {

	public GeneralIntegerSettingButton(String name, EventwarpSettings settings, int minvalue, int maxvalue) {
		super(name, settings, minvalue, maxvalue);
	}
	
	@Override
	public void addToPanel(ControlPanel panel) {
		ItemButton button = createItem(panel);
		updateItem(button);
		button.setTask(task);
		panel.setItem(button, getSlot());
	}
	
	@Override
	protected void updateItem(ItemButton button) {
		button.setAmount(value);
		ItemMeta meta = button.getItemMeta();
		meta.setDisplayName(toString());
		button.setItemMeta(meta);
	}
	
	protected abstract ItemButton createItem(ControlPanel panel);
	
	

}
