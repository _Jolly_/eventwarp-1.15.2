package de.jolly.eventwarp.settings;

import org.bukkit.configuration.file.FileConfiguration;

public abstract class IntegerSetting extends Setting {
	
	protected int value;
	
	public IntegerSetting(String name, EventwarpSettings settings) {
		super(name, settings);
	}
	
	/**
	 * Returns the current state of this BooleanSetting
	 */
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		if (this.value == value) {
			return;
		}
		this.value = value;
		getSettings().save();
		onUpdate();
	}
	
	@Override
	public void writeToFile(FileConfiguration cfg, String path) {
		cfg.set(path+"."+getName(), value);
	}
	
	@Override
	public void loadFromFile(FileConfiguration cfg, String path) {
		value = cfg.getInt(path+"."+getName());
		onLoad();
	}
}
