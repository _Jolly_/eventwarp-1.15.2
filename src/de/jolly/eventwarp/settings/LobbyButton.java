package de.jolly.eventwarp.settings;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.inventory.ButtonTask;
import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import de.jolly.eventwarp.controls.Eventwarp;

public class LobbyButton extends ItemButton {

	private static final String LOBBY_MODE_TEXT = "�aIm Lobbymodus";
	private static final String GAME_MODE_TEXT = "�cIm Spielmodus";
	
	private final String permission;
	private final Eventwarp warp;
	
	public LobbyButton(InteractionInventory inv, String permission, Eventwarp warp) {
		super(Material.IRON_SWORD, GAME_MODE_TEXT, inv);
		this.permission = permission;
		this.warp = warp;
		setTask(task);
		update(warp.isLobbyMode());
	}
	
	void update(boolean lobbymode) {
		if (lobbymode) {
			setLobbyState();
		} else {
			setGameState();
		}
	}
	
	private void setLobbyState() {
		setType(Material.COOKIE);
		ItemMeta meta = getItemMeta();
		meta.setDisplayName(LOBBY_MODE_TEXT);
		List<String> lore = new ArrayList<>();
		lore.add("Klicke, um zum Spielmodus zu wechseln.");
		meta.setLore(lore);
		setItemMeta(meta);
	}
	private void setGameState() {
		setType(Material.IRON_SWORD);
		ItemMeta meta = getItemMeta();
		meta.setDisplayName(GAME_MODE_TEXT);
		List<String> lore = new ArrayList<>();
		lore.add("Klicke, um zum Lobbymodus zu wechseln.");
		meta.setLore(lore);
		setItemMeta(meta);
	}
	
	private final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			if (e.getWhoClicked().hasPermission(permission)) {
				warp.setLobbyMode(!warp.isLobbyMode());
			} else {
				((Player)e.getWhoClicked()).playSound(e.getWhoClicked().getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
			}
		}
	};
}
