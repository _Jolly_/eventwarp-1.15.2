package de.jolly.eventwarp.settings;

import org.bukkit.configuration.file.FileConfiguration;

public abstract class BooleanSetting extends Setting {
	
	protected boolean value;
	
	public BooleanSetting(String name, EventwarpSettings settings) {
		super(name, settings);
	}
	
	/**
	 * Returns the current state of this BooleanSetting
	 */
	public boolean isEnabled() {
		return value;
	}
	
	public void setEnabled(boolean value) {
		if (this.value == value) {
			return;
		}
		this.value = value;
		getSettings().save();
		onUpdate();
	}
	
	@Override
	public void writeToFile(FileConfiguration cfg, String path) {
		cfg.set(path+"."+getName(), value);
	}
	
	@Override
	public void loadFromFile(FileConfiguration cfg, String path) {
		value = cfg.getBoolean(path+"."+getName());
		onLoad();
	}
}
