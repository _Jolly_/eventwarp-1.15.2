package de.jolly.eventwarp.settings;

import codetools.inventory.InteractionInventory;
import codetools.inventory.ItemButton;
import codetools.main.EnhancedJavaPlugin;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.event.EventwarpCloseEvent;
import de.jolly.eventwarp.event.EventwarpOpenEvent;
import de.jolly.eventwarp.event.EventwarpPlayerSwitchTeamEvent;
import de.jolly.eventwarp.event.EventwarpToggleLobbymodeEvent;
import de.jolly.eventwarp.external.EventwarpListener;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.Team;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ControlPanel extends InteractionInventory implements Listener, EventwarpListener {

	private final int[] SLOTS_BUTTON_OPEN = new int[] { 18, 19, 27, 28 };
	private final int[] SLOTS_TEAMS = new int[] { 2, 3, 4, 11, 12, 13 };
	private final int SLOT_BUTTON_KIT = 46;
	private final int SLOT_BUTTON_ARMOR = 45;
	private final int SLOT_BUTTON_PLAYER_GUI = 0;
	private final int SLOT_BUTTON_LOBBY = 30;

	private final String TEAM_CLOSED = "�cTeam inaktiv";
	
	private final EnhancedJavaPlugin main;
	private final Eventwarp warp;
	
	ControlPanel(EnhancedJavaPlugin main, Eventwarp warp) {
		super(54, "�3�lEventwarp");
		this.main = main;
		this.warp = warp;
	}

	@Override
	public void showToPlayer(Player p) {
		final String permission = main.permission()+".controlpanel";
		if (p.hasPermission(permission)) {
			super.showToPlayer(p);
		} else {
			main.sendMessage(p, "�cDu ben�tigst die Permission �defS"+permission+"�c f�r diese Aktion!");
		}
	}

	/**
	 * Creates additional buttons from the Eventwarp, which
	 * e.g. show whether or not the warp is currently open.
	 * @param warp The Eventwarp from which to take the data.
	 */
	public void createAdditionalButtons(final EventwarpMain main, final Eventwarp warp) {
		ItemButton open = new OpenButton(warp, this);
		for (int j : SLOTS_BUTTON_OPEN) {
			setItem(open, j);
		}
		setStatusOpen(warp.isOpen());


		ItemButton kitButton = new ItemButton(Material.DIAMOND_SWORD, "�bInventar bearbeiten", (button, e) -> {
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand(e.getWhoClicked(), main.command()+" kit");
		}, this);
		setItem(kitButton, SLOT_BUTTON_KIT);
		ItemButton armorButton = new ItemButton(Material.DIAMOND_CHESTPLATE, "�bR�stung bearbeiten", (button, e) -> {
			e.getWhoClicked().closeInventory();
			Bukkit.dispatchCommand(e.getWhoClicked(), main.command()+" armor");
		}, this);
		setItem(armorButton, SLOT_BUTTON_ARMOR);


		ItemButton playerManagerGUIButton = new ItemButton(Material.PLAYER_HEAD, "�3�lSpieler", (button, e) -> {
			e.getWhoClicked().closeInventory();
			warp.getPlayerManager().getGUI().openInventory((Player)e.getWhoClicked());
		}, this);
		ItemMeta pmbmeta = playerManagerGUIButton.getItemMeta();
		List<String> lore = new ArrayList<>();
		for (EventPlayer all: warp.getPlayerManager().getAllPlayers()) {
			lore.add(all.getName());
		}
		pmbmeta.setLore(lore);
		playerManagerGUIButton.setItemMeta(pmbmeta);
		setItem(playerManagerGUIButton, SLOT_BUTTON_PLAYER_GUI);
		
		
		ItemButton lobbyButton = new LobbyButton(this, main.permission()+".lobbymode", warp);
		setItem(lobbyButton, SLOT_BUTTON_LOBBY);
		
		
		Team[] teams = Team.values();
		for (int i = 0; i < teams.length && i < SLOTS_TEAMS.length; i++) {
			ItemStack item = new ItemStack(teams[i].getWoolColorType());
			setItem(item, SLOTS_TEAMS[i]);
			reloadTeamLore(teams[i]);
		}
	}

	private void setStatusOpen(boolean open) {
		for (int j : SLOTS_BUTTON_OPEN) {
			ItemStack item = getItem(j);
			if (item != null) {
				((OpenButton) item).updateMetaForState(open);
			}
		}
		update();
	}
	private void addPlayerToLore(String name) {
		ItemStack playerManagerGUIButton = getItem(SLOT_BUTTON_PLAYER_GUI);
		if (playerManagerGUIButton != null) {
			ItemMeta pmbmeta = playerManagerGUIButton.getItemMeta();
			List<String> lore = pmbmeta.getLore();
			lore.add(name);
			pmbmeta.setLore(lore);
			playerManagerGUIButton.setItemMeta(pmbmeta);
		}
	}
	private void removePlayerFromLore(String name) {
		ItemStack playerManagerGUIButton = getItem(SLOT_BUTTON_PLAYER_GUI);
		if (playerManagerGUIButton != null) {
			ItemMeta pmbmeta = playerManagerGUIButton.getItemMeta();
			List<String> lore = pmbmeta.getLore();
			lore.remove(name);
			pmbmeta.setLore(lore);
			playerManagerGUIButton.setItemMeta(pmbmeta);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onOpen(EventwarpOpenEvent e) {
		if (warp == e.getEventwarp() && !e.isCancelled()) {
			setStatusOpen(true);
		}
	}
	@EventHandler(priority = EventPriority.MONITOR)
	public void onClose(EventwarpCloseEvent e) {
		if (warp == e.getEventwarp()) {
			setStatusOpen(false);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onLobbyModeSwitched(EventwarpToggleLobbymodeEvent e) {
		if (warp == e.getEventwarp()) {
			((LobbyButton) getItem(SLOT_BUTTON_LOBBY)).update(e.getLobbymodeEnabled());
		}
	}
	
	@Override
	public void onJoin(EventPlayer player) {
		addPlayerToLore(player.getName());
		update();
	}

	@Override
	public void onLeave(EventPlayer player) {
		removePlayerFromLore(player.getName());
		if (player.getTeam() != null) {
			reloadTeamLore(player.getTeam());
		}
		update();
	}

	public void reloadTeamLore(Team team) {
		int teamIndex = getTeamIndex(team);
		ItemStack teamitem = getItem(SLOTS_TEAMS[teamIndex]);
		ItemMeta meta = teamitem.getItemMeta();
		EventPlayer[] players = warp.getPlayerManager().getPlayersByTeam(team);
		meta.setDisplayName(team.getChatColor()+"Team "+team.getChatName());
		List<String> lore = new ArrayList<>(players.length+1);
		for (EventPlayer player: players) {
			lore.add("�b"+player.getName());
		}
		if (teamIndex >= warp.getPlayerManager().getActiveTeams())
			lore.add(TEAM_CLOSED);
		meta.setLore(lore);
		teamitem.setItemMeta(meta);
	}

	@EventHandler
	public void onTeamSwitch(EventwarpPlayerSwitchTeamEvent e) {
		if (warp == e.getEventwarp()) {
			if (e.getOldTeam() != null) {
				reloadTeamLore(e.getOldTeam());
			}
			if (e.getPlayer().getTeam() != null) {
				reloadTeamLore(e.getPlayer().getTeam());
			}
			
			if (System.currentTimeMillis()-cancelUpdateTimer > 20) {
				update();
			}
		}
	}
	
	@Override
	public void onRespawn(EventPlayer player) {}

	@Override
	public void onDeath(EventPlayer player) {
	}
	
	private int getTeamIndex(Team t) {
		Team[] all = Team.values();
		for (int i = 0; i < all.length; i++) {
			if (all[i] == t) return i;
		}
		return -1;
	}
	
	/**
	 * This is needed to avoid spamming the update() function
	 * when all players are assigned to teams at once.
	 */
	private long cancelUpdateTimer = 0;
	
	/**
	 * Do not use this method. It marks which teams are open for being joined.
	 * It's the PlayerManager's job to call this method.
	 */
	public void setActiveTeams(int amount) {
		cancelUpdateTimer = System.currentTimeMillis();
		for (int i = 0; i < SLOTS_TEAMS.length; i++) {
			ItemStack item = getItem(SLOTS_TEAMS[i]);
			ItemMeta meta = item.getItemMeta();
			List<String> lore = meta.getLore();
			if (lore == null) lore = new ArrayList<>(1);
			if (i < amount) {
				lore.remove(TEAM_CLOSED);
			} else {
				if (!lore.contains(TEAM_CLOSED)) {
					if (lore.size() == 0) {
						lore.add(TEAM_CLOSED);
					} else {
						String objAt0 = lore.set(0, TEAM_CLOSED);
						lore.add(objAt0);
					}
				}
			}
			meta.setLore(lore);
			item.setItemMeta(meta);
		}
	}
}
