package de.jolly.eventwarp.settings;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.ItemButton;

public abstract class BooleanSettingButton extends BooleanSetting {
	
	public BooleanSettingButton(String name, EventwarpSettings settings) {
		super(name, settings);
	}
	
	@Override
	public void addToPanel(ControlPanel panel) {
		ItemButton button = new ItemButton(Material.STONE, panel);
		initializeItem(button);
		updateItem(button);
		button.setTask(task);
		panel.setItem(button, getSlot());
	}
	/**
	 * This returns the slot in which the ItemButton shall be placed.
	 */
	protected abstract int getSlot();
	
	/**
	 * Updates the item's state from the current
	 * state of the setting.
	 */
	protected abstract void updateItem(ItemButton button);
	
	/**
	 * Initializes the Item.
	 * Here are modifications done to the item that will
	 * always be the same, independently from the state
	 * of the setting.
	 * This method exists only for the option to overwrite it,
	 * it does nothing by default.
	 */
	protected void initializeItem(ItemButton button) {
		
	}
	
	protected final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			setEnabled(!isEnabled());
			updateItem(button);
			getSettings().getControlPanel().update();
		}
	};
}
