package de.jolly.eventwarp.settings;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.inventory.InventoryClickEvent;

import codetools.inventory.ButtonTask;
import codetools.inventory.ItemButton;

public abstract class IntegerSettingButton extends IntegerSetting {
	
	/**
	 * The maximum allowed value for this IntegerSetting, including
	 */
	private int maxvalue;
	/**
	 * The minimum allowed value for this IntegerSetting, including
	 */
	private int minvalue;
	
	public IntegerSettingButton(String name, EventwarpSettings settings, int minvalue, int maxvalue) {
		super(name, settings);
		setValueLimits(minvalue, maxvalue);
	}
	
	/**
	 * Sets the maximum and minimum values allowed for this
	 * IntegerSetting.
	 */
	public void setValueLimits(int min, int max) {
		if (max < min) {
			return;
		}
		
		maxvalue = max;
		minvalue = min;
		
		if (value > max) {
			setValue(max);
		}
		if (value < min) {
			setValue(min);
		}
	}
	
	@Override
	public void loadFromFile(FileConfiguration cfg, String path) {
		super.loadFromFile(cfg, path);
		
		if (value > maxvalue) {
			setValue(maxvalue);
		}
		if (value < minvalue) {
			setValue(minvalue);
		}
	}
	
	@Override
	public void addToPanel(ControlPanel panel) {
		ItemButton button = new ItemButton(Material.STONE, panel);
		updateItem(button);
		button.setTask(task);
		panel.setItem(button, getSlot());
	}
	/**
	 * This returns the slot in which the ItemButton shall be placed.
	 */
	protected abstract int getSlot();
	
	protected abstract void updateItem(ItemButton button) ;
	
	protected final ButtonTask task = new ButtonTask() {
		@Override
		public void execute(ItemButton button, InventoryClickEvent e) {
			if (e.isLeftClick()) {
				int value = IntegerSettingButton.this.value+1;
				if (value > maxvalue)  {
					return;
				}
				setValue(value);
			} else {
				int value = IntegerSettingButton.this.value-1;
				if (value < minvalue)  {
					return;
				}
				setValue(value);
			}
			updateItem(button);
			getSettings().getControlPanel().update();
		}
	};
	
}
