package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.PlayerManager;

public class RespawnAllCommand extends AdvancedCommand {

	private final PlayerManager manager;
	
	public RespawnAllCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);
		
		this.manager = manager;
		
		callArgument = "respawnall";
		argumentDescription = "";
		description = "Respawnt alle Spieler.";
		permission = "respawn";
		isAdvanced = true;
		consoleAllowed = true;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		manager.respawnAllPlayers();
		sendMessage(sender, "Alle Spieler wurden respawnt!");
	}

}
