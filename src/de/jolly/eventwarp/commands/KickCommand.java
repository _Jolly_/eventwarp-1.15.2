package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;

public class KickCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public KickCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "kick";
		argumentDescription = "<name>";
		description = "Kickt einen Spieler vom Eventwarp.";
		permission = "kick";
		isAdvanced = false;
		consoleAllowed = true;
		
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player target = Bukkit.getPlayerExact(args[0]);
		
		EventPlayer p = warp.getPlayerManager().getPlayer(target);
		if (p == null) {
			sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich nicht im Event.");
			return;
		}
		
		p.kick();
		sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM wurde vom Event gekickt.");
		sendMessage(target, "Du wurdest vom Event gekickt.");
	}

}
