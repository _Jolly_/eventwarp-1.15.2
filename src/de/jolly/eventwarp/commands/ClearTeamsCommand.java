package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.PlayerManager;

public class ClearTeamsCommand extends AdvancedCommand {

private final PlayerManager manager;
	
	public ClearTeamsCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);
		
		this.manager = manager;
		
		callArgument = "clearteams";
		argumentDescription = "";
		description = "Leert alle Teams.";
		permission = "teams";
		isAdvanced = true;
		consoleAllowed = true;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		manager.clearTeams();
		sendMessage(sender, "Die Spieler wurden aus ihren Teams entfernt. Neu hinzukommende Spieler werden keinem Team mehr zugeteilt.");
	}

}
