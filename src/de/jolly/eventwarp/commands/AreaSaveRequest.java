package de.jolly.eventwarp.commands;

class AreaSaveRequest {
	
	private final String areaname;
	private final long requesttime;
	
	AreaSaveRequest(String areaname) {
		this.areaname = areaname;
		requesttime = System.currentTimeMillis();
	}
	
	public long getRequestTime() {
		return requesttime;
	}
	public String getRequestedAreaName() {
		return areaname;
	}
	
}
