package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.external.EventwarpAdapter;
import de.jolly.eventwarp.external.EventwarpPlugin;

public class AttachCommand extends AdvancedCommand {
	
	private Eventwarp warp;
	
	public AttachCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "attach";
		argumentDescription = "<plugin>";
		description = "Verbindet ein Minispiel-Plugin mit dem Eventwarp.";
		permission = "attach";
		isAdvanced = true;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Plugin target = Bukkit.getPluginManager().getPlugin(args[0]);
		if (target == null) {
			sendMessage(sender, "Das Plugin �defS"+args[0]+"�defM wurde nicht gefunden.");
			return;
		}
		if (!(target instanceof EventwarpPlugin)) {
			sendMessage(sender, "Das Plugin �defS"+target.getName()+"�defM ist kein passendes Plugin f�r diese Aktion.");
			return;
		}
		
		EventwarpPlugin plugin = (EventwarpPlugin) target;
		EventwarpAdapter adapter = plugin.getEventwarpAdapter();
		if (adapter == null) {
			sendMessage(sender, "�cFehler! Das Plugin �4"+target.getName()+"�c besitzt keinen korrekten EventwarpAdapter. �e(->Beim Entwickler beschweren!)");
			return;
		}
		warp.attachAdapter(adapter);
		sendMessage(sender, "Das Plugin �defS"+target.getName()+"�defM wurde erfolgreich mit dem Eventwarp verbunden. Die Konfiguration wurde neu geladen.");
		
		if (warp.getPlayerManager().getAllPlayers().length > 0) {
			sendMessage(sender, "Hinweis: Es befinden sich noch Spieler im Event. M�glicherweise verursacht dies bei diesem Plugin Probleme. Falls das in der Vergangenheit zutraf, empfehle ich, jetzt alle Spieler vom Eventwarp zu kicken und neu beitreten zu lassen.");
		}
		
	}

}
