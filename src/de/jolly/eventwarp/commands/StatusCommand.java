package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import codetools.main.CodeTools;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.controls.SpawnLocationManager;
import de.jolly.eventwarp.players.EventPlayer;

public class StatusCommand extends AdvancedCommand {
	
	private Eventwarp warp;

	public StatusCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "status";
		argumentDescription = "[player]";
		description = "Zeigt den Status des Eventwarps oder eines Spielers an.";
		permission = "status";
		isAdvanced = false;
		consoleAllowed = true;
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length > 0) {
			
			EventPlayer p = warp.getPlayerManager().getPlayer(args[0]);
			if (p != null) {
				StringBuilder message = new StringBuilder("�aStatus des Spielers �defS"+p.getName()+"�a:");
				for (String s: p.getStatus()) {
					message.append("\n�9"+s);
				}
				sendMessage(sender, message.toString());
			} else {
				sendMessage(sender, "Der Spieler �defS"+Bukkit.getPlayerExact(args[0]).getName()+"�defM ist nicht im Event.");
			}
			
		} else {
			sendMessage(sender, createStatusMessage());
		}
	}
	
	private String createStatusMessage() {
		StringBuilder message = new StringBuilder("�aStatus des Eventwarps:");
		if (warp.isOpen()) {
			message.append("\n�9Zugang: �aAktuell ge�ffnet");
		} else {
			message.append("\n�9Zugang: �cAktuell geschlossen");
		}
		message.append("\n�9Aktuelle Spielerzahl: �a"+warp.getPlayerManager().getAllPlayers().length);
		message.append("\n�9Anzahl der Normalen Spieler-Spawnpunkte: �a"+warp.getSpawnManager().getAllLocations(SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP).length);
		message.append("\n�9Anzahl der Spectator-Spawnpunkte: �a"+warp.getSpawnManager().getAllLocations(SpawnLocationManager.DEFAULT_SPECTATOR_SPAWN_GROUP).length);
		message.append("\n�9Aktuell verbundenes Plugin: �a");
		if (warp.isDefaultAdapter()) {
			message.append("keines");
		} else {
			message.append(warp.getAdapter().getMain().getName());
		}
		message.append("\n�9Aktuelle Spieler: �e"+CodeTools.toString(warp.getPlayerManager().getAllPlayers()));
		
		return message.toString();
	}

}
