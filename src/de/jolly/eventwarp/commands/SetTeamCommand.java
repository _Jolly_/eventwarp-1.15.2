package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.players.Team;

public class SetTeamCommand extends AdvancedCommand {

	private PlayerManager manager;

	public SetTeamCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);

		this.manager = manager;

		callArgument = "setteam";
		argumentDescription = "<name> <team|none>";
		description = "Weist einen Spieler einem Team zu.";
		permission = "teams";
		isAdvanced = true;
		consoleAllowed = true;

		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player target = Bukkit.getPlayerExact(args[0]);

		EventPlayer p = manager.getPlayer(target);
		if (p == null) {
			sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich nicht im Event.");
			return;
		}

		Team targetteam = Team.fromCharacter(args[1].charAt(0));
		if (targetteam == null && !args[1].equalsIgnoreCase("none")) {
			sendMessage(sender, "Das Team �defS"+args[1]+"�defM konnte nicht erkannt werden. �6(yellow|blue|red|green|white|purple|none)");
			return;
		}

		if (targetteam == null) {
			
			if (targetteam == p.getTeam()) {
				sendMessage(sender, "Der Spieler �defS"+p.getName()+"�defM ist bereits in keinem Team.");
				return;
			}

			p.setTeam(targetteam);

			sendMessage(sender, "�defS"+target.getName()+"�defM wurde aus seinem Team entfernt.");
			sendMessage(target, "Du wurdest von �defS"+sender.getName()+"�defM aus deinem Team entfernt.");
			
		} else {

			if (targetteam == p.getTeam()) {
				sendMessage(sender, "Der Spieler �defS"+p.getName()+"�defM ist bereits in diesem Team.");
				return;
			}

			p.setTeam(targetteam);

			sendMessage(sender, "�defS"+target.getName()+"�defM wurde zum Team "+targetteam.getChatName()+"�defM hinzugef�gt.");
			sendMessage(target, "Du wurdest von �defS"+sender.getName()+"�defM zum Team "+targetteam.getChatName()+"�defM hinzugef�gt.");
			
		}
	}

}
