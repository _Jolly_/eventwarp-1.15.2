package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;

public class CloseCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public CloseCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "close";
		argumentDescription = "";
		description = "Schlie�t den Eventwarp.";
		permission = "open";
		isAdvanced = false;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!warp.isOpen()) {
			sendMessage(sender, "Der Eventwarp ist nicht offen.");
		} else {
			warp.setOpen(false);
			sendMessage(sender, "Der Eventwarp wurde geschlossen!");
		}
		
	}

}
