package de.jolly.eventwarp.commands;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.region.MarkedArea;

public class AreaSaveCommand extends AdvancedCommand {

	private final Map<CommandSender, AreaSaveRequest> activeRequests;
	private final int CONFIRMATION_TIME = 10000;
	private final String PATH;

	public AreaSaveCommand(EventwarpMain main, CommandsHub hub) {
		super(hub);
		
		PATH = main.getDataFolder()+File.separator+"savedareas";

		callArgument = "savearea";
		argumentDescription = "<name>";
		description = "Speichert den von dir markieren Bereich.";
		permission = "areamarker";
		isAdvanced = true;
		consoleAllowed = false;

		activeRequests = new HashMap<>();
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		MarkedArea area = AreaMarkerCommand.currentAreas.get((Player) sender);
		if (area == null || !area.isValid()) {
			sendMessage(sender, "Du hast keinen g�ltigen Bereich markiert!");
		} else {
			File f = new File(PATH+File.separator+args[0]+"."+MarkedArea.FILE_ENDING);
			if (f.exists()) {
				
				AreaSaveRequest request = activeRequests.get(sender);
				if (request != null && System.currentTimeMillis() - request.getRequestTime() <= CONFIRMATION_TIME) {
					if (request.getRequestedAreaName().equals(args[0])) {
						saveArea(sender, area, args[0]);
					} else {
						sendMessage(sender, "Du hast verschiedene Namen zum Speichern angegeben �6(�defS"+request.getRequestedAreaName()+" =/= "+args[0]+"�6)�defM! Versuche es erneut.");
					}
					activeRequests.remove(sender);
				} else {
					activeRequests.put(sender, new AreaSaveRequest(args[0]));
					sendMessage(sender, "�cEine Region mit dem Namen �defS"+args[0]+"�c existiert bereits!\n"
							+"Wenn du sicher bist, dass du diese Region mit dem von dir markieren Bereich �berschreiben willst,"
							+" benutze diesen Command �6(�a/eventwarp savearea "+args[0]+"�6)�defM innerhalb von 10 Sekunden erneut.");
				}
				
			} else {
				saveArea(sender, area, args[0]);
			}
		}
	}
	
	private void saveArea(CommandSender sender, MarkedArea area, String name) {
		sendMessage(sender, "Speichere markierten Bereich...");
		File dir = new File(PATH);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				sendMessage(sender, "Der erforderliche Ordner konnte nicht erstellt werden.");
				return;
			}
		}
		long starttime = System.currentTimeMillis();
		area.saveToFiles(PATH+File.separator+name);
		sendMessage(sender, "Der Bereich wurde unter dem Namen �defS"+name+"�defM gespeichert! �6("+(System.currentTimeMillis()-starttime)+"ms)");
	}
	
}
