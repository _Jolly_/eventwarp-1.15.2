package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;

public class LeaveCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public LeaveCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "leave";
		argumentDescription = "";
		description = "Verl�sst den Eventwarp.";
		permission = "";
		isAdvanced = false;
		consoleAllowed = false;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		
		EventPlayer e = warp.getPlayerManager().getPlayer(p);
		if (e == null) {
			sendMessage(sender, "Du befindest dich nicht im Event!");
			return;
		}
		
		warp.getPlayerManager().kickPlayer(e);
	}

}
