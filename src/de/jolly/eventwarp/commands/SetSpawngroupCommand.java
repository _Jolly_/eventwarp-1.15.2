package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;

public class SetSpawngroupCommand extends AdvancedCommand {

	private PlayerManager manager;

	public SetSpawngroupCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);

		this.manager = manager;

		callArgument = "setspawngroup";
		argumentDescription = "<name> <spawngruppe>";
		description = "Stellt die Spawngruppe eines Spielers ein.";
		permission = "spawngroups";
		isAdvanced = true;
		consoleAllowed = true;

		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player target = Bukkit.getPlayerExact(args[0]);

		EventPlayer p = manager.getPlayer(target);
		if (p == null) {
			sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich nicht im Event.");
			return;
		}
		
		p.setSpawngroup(args[1]);
		sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM wurde zur Spawngruppe �d"+args[1]+"�defM hinzugef�gt.");
		
	}

}
