package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import codetools.inventory.InteractionInventory;
import de.jolly.eventwarp.controls.Eventwarp;

public class ArmorCommand extends AdvancedCommand {

	private Eventwarp warp;

	public ArmorCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);

		this.warp = warp;

		callArgument = "armor";
		argumentDescription = "";
		description = "Stelle die R�stung neu ein.";
		permission = "equipment";
		isAdvanced = true;
		consoleAllowed = false;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		InteractionInventory inv = warp.getEventEquipment().getArmorInventory();
		
		inv.showToPlayer(p);

		//Speichern funktioniert in der Klasse EventEquipment
	}

}
