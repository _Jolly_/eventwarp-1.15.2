package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.Team;

public class SpawngroupInfoCommand extends AdvancedCommand {
	
	public SpawngroupInfoCommand(CommandsHub hub) {
		super(hub);

		callArgument = "spawngroups";
		argumentDescription = "";
		description = "Zeigt Informationen zu den Spawngruppen.";
		permission = "spawngroups";
		isAdvanced = true;
		consoleAllowed = true;

		hub.registerCommandAlias(this, "spawngroupinfo");
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		StringBuilder teamspawnpoints = new StringBuilder("\n�bStandardgruppen f�r Teams:");
		for (Team t: Team.values()) {
			teamspawnpoints.append("\n  "+t.getChatName()+": "+toSpawnGroupStringFormat(t.getDefaultSpawngroup()));
		}
		sendMessage(sender, "Spawngruppen legen fest, welche Spawnpunkte f�r welche Spieler verf�gbar sind."
				+" Jeder eingestelle Spawnpunkt ist einer Spawngruppe zugeordnet, ebenso ist jeder Spieler einer Spawngruppe zugeordnet."
				+" F�r jeden Spieler stehen alle Spawnpunkte seiner Spawngruppe zur Verf�gung, von diesen wird zuf�llig einer gew�hlt."
				+" Ist ein Spieler in einer Spawngruppe, die keine Spawnpunkte besitzt, so nutzt er automatisch die Spawnpunkte der normalen Spieler."
				+" Zudem sind Spawngruppen �ccase-sensitive�defM!"
				+"\n�aDie Standardspawngruppen lauten wie folgt:"
				+"\n�bStandardgruppe f�r Spieler: "+toSpawnGroupStringFormat(de.jolly.eventwarp.controls.SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP)
				+"\n�bStandardgruppe f�r Spectator: "+toSpawnGroupStringFormat(de.jolly.eventwarp.controls.SpawnLocationManager.DEFAULT_SPECTATOR_SPAWN_GROUP)
				+"\n�bLobby-Spawngruppe: "+toSpawnGroupStringFormat(de.jolly.eventwarp.controls.SpawnLocationManager.LOBBY_SPAWN_GROUP)
				+teamspawnpoints.toString());
	}
	
	public static String toSpawnGroupStringFormat(String spawngroup) {
		return "�9[�d"+spawngroup+"�9]";
	}
}
