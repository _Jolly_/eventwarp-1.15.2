package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.players.EventPlayer;

public class AddCommand extends AdvancedCommand {

	private Eventwarp warp;
	private EventwarpMain main;
	
	public AddCommand(Eventwarp warp, EventwarpMain main, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		this.main = main;
		
		callArgument = "add";
		argumentDescription = "<name>";
		description = "F�gt einen Spieler zum Eventwarp hinzu.";
		permission = "add";
		isAdvanced = true;
		consoleAllowed = true;
		
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player target = Bukkit.getPlayerExact(args[0]);
		
		EventPlayer p = warp.getPlayerManager().getPlayer(target);
		if (p != null) {
			sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich bereits im Event.");
			return;
		}
		
		if (main.getBanlist().isBanned(target)) {
			sendMessage(sender, "�defS"+target.getName()+"�defM ist vom Eventwarp gebannt.");
			if (sender.hasPermission(main.permission()+".bypassban")) {
				sendMessage(sender, "Bann wird ignoriert.");
			} else {
				return;
			}
		}
		if (!warp.isOpen()) {
			if (sender.hasPermission(main.permission()+".bypassclosed")) {
				sendMessage(sender, "Der Eventwarp ist geschlossen. Deine Rechte erzwingen dennoch den Beitritt.");
			} else {
				sendMessage(sender, "Der Eventwarp ist derzeit geschlossen.");
				return;
			}
		}
		
		warp.getPlayerManager().addPlayer(target);
		sendMessage(sender, "�defS"+target.getName()+"�defM wurde zum Event hinzugef�gt.");
		sendMessage(target, "Du wurdest von �defS"+sender.getName()+"�defM zum Event hinzugef�gt.");
	}

}
