package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;

public class EnableGameModeCommand extends AdvancedCommand {

private Eventwarp warp;
	
	public EnableGameModeCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "game";
		argumentDescription = "";
		description = "Wechselt zum Spielmodus";
		permission = "lobbymode";
		isAdvanced = true;
		consoleAllowed = true;
		
		hub.registerCommandAlias(this, "gamemode");
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!warp.isLobbyMode()) {
			sendMessage(sender, "Der Spielmodus ist bereits aktiv.");
			return;
		}
		warp.setLobbyMode(false);
		sendMessage(sender, "�cDer Lobbymodus wurde deaktiviert und es wurde zum Spielmodus gewechselt.");
	}

}
