package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.players.Team;

public class AssignTeamsCommand extends AdvancedCommand {
	
	private final PlayerManager manager;
	
	public AssignTeamsCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);
		
		this.manager = manager;
		
		callArgument = "assignteams";
		argumentDescription = "<anzahl>";
		description = "Verteilt alle Spieler gleichm��ig in die angegebene Anzahl an Teams.";
		permission = "teams";
		isAdvanced = true;
		consoleAllowed = true;
		
		argFormat = new ArgumentFormat[] { ArgumentFormat.INTEGER };
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		int amount = Integer.parseInt(args[0]);
		if (amount < 1 || amount > Team.values().length) {
			sendMessage(sender, "Die Anzahl an ausgew�hlten Teams muss zwischen �defS1�defM und �defS"+Team.values().length+"�defM liegen.");
			return;
		}
		manager.assignTeams(amount);
		if (amount == 1) {
			sendMessage(sender, "Alle Spieler wurden ins �defSTeam 1�defM verschoben.");
		} else {
			sendMessage(sender, "Die Spieler wurden auf �defS"+amount+" Teams�defM verteilt.");
		}
	}

}
