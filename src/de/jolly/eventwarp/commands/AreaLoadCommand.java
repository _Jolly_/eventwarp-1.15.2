package de.jolly.eventwarp.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import codetools.main.CodeTools;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.region.AreaDataException;
import de.jolly.eventwarp.region.MarkedArea;

public class AreaLoadCommand extends AdvancedCommand {
	
	private EventwarpMain main;
	private final String PATH;
	
	public AreaLoadCommand(EventwarpMain main, CommandsHub hub) {
		super(hub);
		
		this.main = main;
		PATH = main.getDataFolder()+File.separator+"savedareas";
		
		callArgument = "loadarea";
		argumentDescription = "[name]";
		description = "Setzt einen festgelegten Bereich auf einen gespeicherten Zustand zur�ck. Wird kein Name angegeben, werden die gespeicherten Bereiche angezeigt.";
		permission = "loadarea";
		isAdvanced = true;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			File dir = new File(PATH);
			File[] files = dir.listFiles();
			if (files == null || files.length == 0) {
				sendMessage(sender, "Es sind keine Bereiche gespeichert.");
				return;
			}
			List<String> names = new ArrayList<>();
			for (File f: files) {
				if (f.getName().endsWith(MarkedArea.FILE_ENDING)) {
					names.add(f.getName().substring(0, f.getName().length()-MarkedArea.FILE_ENDING.length()-1));
				}
			}
			sendMessage(sender, "�aGespeicherte Bereiche:\n�e"+CodeTools.toString(names.toArray(new String[names.size()])));
		} else {
			File f = new File(PATH+File.separator+args[0]+"."+MarkedArea.FILE_ENDING);
			if (!f.exists()) {
				sendMessage(sender, "Es ist kein Bereich unter dem Namen �defS"+args[0]+"�defM gespeichert.");
				return;
			}
			try {
				sendMessage(sender, "Bereich wird wiederhergestellt...");
				long starttime = System.currentTimeMillis();
				MarkedArea.rebuildFromFiles(PATH+File.separator+args[0]);
				sendMessage(sender, "Bereich erfolgreich zur�ckgesetzt! �6("+(System.currentTimeMillis()-starttime)+"ms)");
			} catch (AreaDataException e) {
				sendMessage(sender, "�cR�cksetzen des Bereichs fehlgeschlagen. Weitere Infos in der Konsole.");
				main.getErrorHandler().ExceptionOccured(e);
			}
		}
	}

}
