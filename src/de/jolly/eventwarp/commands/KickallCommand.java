package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;

public class KickallCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public KickallCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "kickall";
		argumentDescription = "";
		description = "Kickt alle Spieler vom Eventwarp.";
		permission = "kick";
		isAdvanced = false;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		warp.getPlayerManager().kickAllPlayers();
		sendMessage(sender, "Alle Spieler wurden vom Event gekickt.");
		if (warp.isOpen()) {
			warp.setOpen(false);
			sendMessage(sender, "Der Eventwarp wurde geschlossen.");
		}
	}

}
