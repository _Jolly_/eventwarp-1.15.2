package de.jolly.eventwarp.commands;

import java.text.DecimalFormat;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.SpawnLocationManager;

/**
 * This class is stolen from the OITC-Plugin, with slight changes.
 * @author _Jolly_
 *
 */
public class SpawnLocationCommand extends AdvancedCommand {

	private SpawnLocationManager manager;

	public SpawnLocationCommand(SpawnLocationManager manager, CommandsHub hub) {
		super(hub);

		this.manager = manager;

		callArgument = "spawn";
		argumentDescription = "[list|remove] [index] [spawngruppe]";
		description = "F�gt deine aktuelle Position als m�glichen Spawnpunkt hinzu.";
		permission = "spawnpoints";
		isAdvanced = true;
		consoleAllowed = false;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		String spawngroup = SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP;
		if (args.length > 0) {
			if (args[0].equalsIgnoreCase("list")) {
				if (args.length > 1) {
					spawngroup = args[1];
				}
				StringBuilder message = new StringBuilder("�aSpawnpunkte �9[�d"+spawngroup+"�9]�a:");
				
				int counter = 1;
				DecimalFormat f = new DecimalFormat("0.0#");
				for (Location all: manager.getAllLocations(spawngroup)) {
					message.append("\n�a["+counter+"] �b"
							+"  X: "+f.format(all.getX())
							+"  Y: "+f.format(all.getY())
							+"  Z: "+f.format(all.getZ()));
					counter++;
				}
				
				sendMessage(sender, message.toString());
				return;
			}
			else if (args[0].equalsIgnoreCase("remove")) {
				if (args.length > 1) {
					if (ArgumentFormat.INTEGER.matchesConditions(args[1])) {
						int index = Integer.parseInt(args[1])-1;
						if (args.length > 2) {
							spawngroup = args[2];
						}
						if (index < 0 || index >= manager.getAllLocations(spawngroup).length) {
							sendMessage(sender, "In der Spawngruppe �9[�d"+spawngroup+"�9]�defM gibt es keinen Spawnpunkt mit dieser Nummer.");
						} else {
							manager.removeLocation(index, spawngroup);
							sendMessage(sender, "Die Spawnposition wurde aus der Spawngruppe �9[�d"+spawngroup+"�9]�defM gel�scht.");
						}
						
					} else {
						sendMessage(sender, ArgumentFormat.INTEGER.getFailureMessage(args[1]));
					}
				} else {
					sendMessage(sender, "Du musst angeben, welcher Spawnpunkt gel�scht werden soll!");
				}
				return;
			}
			spawngroup = args[0];
		}
		
		manager.addLocation(p.getLocation(), spawngroup);
		sendMessage(sender, "Deine Position wurde als Spawnpunkt �9[�d"+spawngroup+"�9]�defM hinzugef�gt.");
	}
}
