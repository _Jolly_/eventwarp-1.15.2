package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.controls.SpawnLocationManager;

public class EnableLobbyModeCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public EnableLobbyModeCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "lobby";
		argumentDescription = "";
		description = "Wechselt zum Lobbymodus";
		permission = "lobbymode";
		isAdvanced = true;
		consoleAllowed = true;
		
		hub.registerCommandAlias(this, "lobbymode");
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (warp.isLobbyMode()) {
			sendMessage(sender, "Der Lobbymodus ist bereits aktiv.");
			return;
		}
		if (!warp.getSpawnManager().hasLocations(SpawnLocationManager.LOBBY_SPAWN_GROUP)) {
			sendMessage(sender, "Daf�r muss mindestens ein Lobbyspawnpunkt existieren!"
					+ " �6(Spawngruppe: "+SpawngroupInfoCommand.toSpawnGroupStringFormat(SpawnLocationManager.LOBBY_SPAWN_GROUP)+"�6)");
			return;
		}
		warp.setLobbyMode(true);
		sendMessage(sender, "�cDer Lobbymodus wurde aktiviert.");
	}
}
