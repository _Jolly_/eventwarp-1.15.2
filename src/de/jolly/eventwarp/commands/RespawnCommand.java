package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;

public class RespawnCommand extends AdvancedCommand {

	private final PlayerManager manager;
	
	public RespawnCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);
		
		this.manager = manager;
		
		callArgument = "respawn";
		argumentDescription = "<player>";
		description = "Respawnt den Spieler.";
		permission = "respawn";
		isAdvanced = true;
		consoleAllowed = true;
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		EventPlayer p = manager.getPlayer(args[0]);
		if (p == null) {
			sendMessage(sender, "Der Spieler �defS"+Bukkit.getPlayerExact(args[0]).getName()+"�defM ist nicht im Event.");
			return;
		}
		p.respawn(true);
		sendMessage(sender, "Der Spieler �derS"+p.getName()+"�defM wurde respawnt!");
	}

}
