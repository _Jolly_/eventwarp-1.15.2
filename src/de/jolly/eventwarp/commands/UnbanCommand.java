package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.main.Banlist;

public class UnbanCommand extends AdvancedCommand {

	private Banlist banlist;
	
	public UnbanCommand(Banlist banlist, CommandsHub hub) {
		super(hub);
		
		this.banlist = banlist;
		
		callArgument = "unban";
		argumentDescription = "<name>";
		description = "Entbannt einen Spieler.";
		permission = "ban";
		isAdvanced = true;
		consoleAllowed = true;
		
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = Bukkit.getPlayerExact(args[0]);
		if (banlist.isBanned(p)) {
			banlist.unbanPlayer(p);
			sendMessage(sender, "Der Spieler �defS"+p.getName()+"�defM wurde vom Eventwarp entbannt.");
		} else {
			sendMessage(sender, "Der Spieler �defS"+p.getName()+"�defM ist nicht vom Eventwarp gebannt.");
		}
	}

}
