package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;

public class DetachCommand extends AdvancedCommand {

	private Eventwarp warp;
	
	public DetachCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "detach";
		argumentDescription = "";
		description = "Entfernt das aktuell verbundene externe Plugin.";
		permission = "attach";
		isAdvanced = true;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (warp.isDefaultAdapter()) {
			sendMessage(sender, "Es ist derzeit kein Plugin mit dem Eventwarp verbunden.");
			return;
		}
		String name = warp.getAdapter().getMain().getName();
		warp.detachAdapter();
		sendMessage(sender, "Die Verbindung mit dem Plugin �defS"+name+"�defM wurde beendet. Die normale Eventwarp-Konfiguration wurde geladen.");
	}

}
