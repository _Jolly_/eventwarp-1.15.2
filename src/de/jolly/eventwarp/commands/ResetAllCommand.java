package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.players.PlayerManager;

public class ResetAllCommand extends AdvancedCommand {
	
	private final PlayerManager manager;
	
	public ResetAllCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);
		
		this.manager = manager;
		
		callArgument = "resetall";
		argumentDescription = "";
		description = "F�llt die Leben, deaktiviert den Spectatormodus und respawnt alle Spieler.";
		permission = "reset";
		isAdvanced = true;
		consoleAllowed = true;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		manager.resetAllPlayers();
		sendMessage(sender, "Alle Spieler wurden resettet!");
	}
	
}
