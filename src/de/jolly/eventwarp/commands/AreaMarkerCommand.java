package de.jolly.eventwarp.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.main.EventwarpMain;
import de.jolly.eventwarp.region.MarkedArea;

public class AreaMarkerCommand extends AdvancedCommand implements Listener {
	
	private final String ITEM_NAME = "§9[§bEventwarp-Areamarker§9]";
	private final Material ITEM_MATERIAL = Material.GOLDEN_SWORD;
	
	private EventwarpMain main;
	
	private boolean listenerActive;
	static Map<Player, MarkedArea> currentAreas;
	
	public AreaMarkerCommand(EventwarpMain main, CommandsHub hub) {
		super(hub);
		
		this.main = main;
		
		callArgument = "areamarker";
		argumentDescription = "";
		description = "Gibt dir ein Item, um einen Bereich zu markieren.";
		permission = "areamarker";
		isAdvanced = true;
		consoleAllowed = false;
		
		listenerActive = false;
		currentAreas = new HashMap<>();
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!listenerActive) {
			main.getServer().getPluginManager().registerEvents(this, main);
			listenerActive = true;
		}
		ItemStack item = new ItemStack(ITEM_MATERIAL);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ITEM_NAME);
		item.setItemMeta(meta);
		((Player)sender).getInventory().addItem(item);
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		ItemStack item = e.getItem();
		if (item == null || item.getType() != ITEM_MATERIAL) return;
		if (item.getItemMeta().getDisplayName() != null && item.getItemMeta().getDisplayName().equals(ITEM_NAME)) {
			if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				if (!currentAreas.containsKey(e.getPlayer())) {
					currentAreas.put(e.getPlayer(), new MarkedArea());
				}
				Block b = e.getClickedBlock();
				World world = b.getWorld();
				int x = b.getX();
				int y = b.getY();
				int z = b.getZ();
				if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
					currentAreas.get(e.getPlayer()).setLoc1(world, x, y, z);
					sendMessage(e.getPlayer(), "Position 1 markiert! §6("+world.getName()+", "+x+", "+y+", "+z+")");
				} else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					currentAreas.get(e.getPlayer()).setLoc2(world, x, y, z);
					sendMessage(e.getPlayer(), "Position 2 markiert! §6("+world.getName()+", "+x+", "+y+", "+z+")");
				}
			}
			e.setCancelled(true);
		}
	}

}
