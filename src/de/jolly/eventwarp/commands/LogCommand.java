package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.EventwarpLog;

public class LogCommand extends AdvancedCommand {
	
	private final EventwarpLog log;
	
	public LogCommand(EventwarpLog log, CommandsHub hub) {
		super(hub);
		
		this.log = log;
		
		callArgument = "log";
		argumentDescription = "[log]";
		description = "Aktiviert die Lognachrichten bzw. zeigt das Log an.";
		permission = "log";
		isAdvanced = true;
		consoleAllowed = true;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("log")) {
			log.showLog(sender);
		} else {
			if (log.isListening(sender)) {
				log.removeListener(sender);
				sendMessage(sender, "Du erh�ltst nun keine geloggten Nachrichten mehr.");
			} else {
				log.addListener(sender);
				sendMessage(sender, "Du erh�ltst nun alle geloggten Nachrichten.");
			}
		}
	}

}
