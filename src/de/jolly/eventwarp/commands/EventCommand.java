package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import codetools.main.EnhancedJavaPlugin;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.main.Banlist;
import de.jolly.eventwarp.players.EventPlayer;

public class EventCommand extends AdvancedCommand {

	private Eventwarp warp;
	private EnhancedJavaPlugin main;
	private Banlist banlist;
	
	public EventCommand(EnhancedJavaPlugin main, Eventwarp warp, Banlist banlist, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		this.main = main;
		this.banlist = banlist;
		callArgument = "";//The callArgument is null, because this command is registered as mainCommand
		argumentDescription = "";
		description = "Tritt dem Eventwarp bei. Befindest du dich im Event, respawnst du mit diesem Command.";
		permission = "";
		isAdvanced = false;
		consoleAllowed = false;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		EventPlayer e = warp.getPlayerManager().getPlayer(p);
		
		if (banlist.isBanned(p) 
				&& e == null) {//This message doesn't have to appear every time you respawn i'd say.
			//If you are banned without special permissions, you can't respawn in the Event as you cannot enter it anyway.
			if (sender.hasPermission(main.permission()+".bypassban")) {
				sendMessage(sender, "Du bist vom Eventwarp gebannt. Deine Rechte umgehen den Bann jedoch.");
			} else {
				sendMessage(sender, "Du bist vom Eventwarp gebannt.");
				return;
			}
		}
		
		if (!warp.isOpen()) {
			if (sender.hasPermission(main.permission()+".bypassclosed")) {
				if (e != null) {
					sendMessage(sender, "Der Eventwarp ist geschlossen. Deine Rechte erlauben trotzdem den Respawn.");
				} else {
					sendMessage(sender, "Der Eventwarp ist geschlossen. Deine Rechte erlauben trotzdem den Beitritt.");
				}
			} else {
				sendMessage(sender, "Der Eventwarp ist derzeit geschlossen.");
				return;
			}
		}
		
		if (e != null) {
			e.respawn(true);
			return;
		}

		warp.getPlayerManager().addPlayer(p);
	}

}
