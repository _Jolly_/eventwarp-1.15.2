package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.main.Banlist;

public class UnbanallCommand extends AdvancedCommand {

	private Banlist banlist;
	
	public UnbanallCommand(Banlist banlist, CommandsHub hub) {
		super(hub);
		
		this.banlist = banlist;
		
		callArgument = "unbanall";
		argumentDescription = "";
		description = "Entbannt alle Spieler.";
		permission = "ban";
		isAdvanced = true;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		banlist.clear();
		sendMessage(sender, "Die Bannliste wurde geleert. Alle Spieler wurden entbannt.");
	}

}
