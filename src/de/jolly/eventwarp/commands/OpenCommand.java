package de.jolly.eventwarp.commands;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.controls.SpawnLocationManager;

public class OpenCommand extends AdvancedCommand {
	
	private Eventwarp warp;
	/**
	 * A list of all players whose command has been halted as the Eventwarp has no warppoint.
	 * The Long value shows the time when the first command has been sent.
	 */
	private Map<CommandSender, Long> commandPending;
	/**
	 * The time a player has to send the command again because it has been halted.
	 */
	private static final int TIMEOUT = 5000;
	
	public OpenCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		
		callArgument = "open";
		argumentDescription = "";
		description = "�ffnet den Eventwarp";
		permission = "open";
		isAdvanced = false;
		consoleAllowed = true;
		
		commandPending = new HashMap<>();
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (warp.isOpen()) {
			sendMessage(sender, "Der Eventwarp ist bereits offen.");
		} else {
			
			if (!warp.getSpawnManager().hasLocations(SpawnLocationManager.DEFAULT_PLAYER_SPAWN_GROUP)) {
				if (commandPending.containsKey(sender)
						&& System.currentTimeMillis() - commandPending.get(sender) < TIMEOUT) {
					commandPending.remove(sender);
				} else {
					commandPending.put(sender, System.currentTimeMillis());
					sendMessage(sender, "�cWarnung: Der Eventwarp besitzt keine normalen Spawnpunkte.�defM Das �ffnen des Eventwarps ist somit unsicher. "
					+"Wenn du dir sicher bist, den Eventwarp �ffnen zu wollen, benutze diesen Befehl innerhalb von "+TIMEOUT/1000+" Sekunden erneut.");
					return;
				}
			}
			
			warp.setOpen(true);
			sendMessage(sender, "Der Eventwarp wurde ge�ffnet!");
		}
		
	}

}
