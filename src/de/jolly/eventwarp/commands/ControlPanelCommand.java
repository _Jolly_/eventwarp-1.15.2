package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.settings.ControlPanel;

public class ControlPanelCommand extends AdvancedCommand {
	
	private ControlPanel panel;
	
	public ControlPanelCommand(ControlPanel panel, CommandsHub hub) {
		super(hub);
		
		this.panel = panel;
		
		callArgument = "controlpanel";
		argumentDescription = "";
		description = "�ffnet das Kontrollpad des Eventwarp.";
		permission = "controlpanel";
		isAdvanced = false;
		consoleAllowed = false;
		
		hub.registerCommandAlias(this, "cp");
		hub.registerCommandAlias(this, "options");
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		panel.showToPlayer(p);
	}

}
