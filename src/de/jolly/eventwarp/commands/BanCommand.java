package de.jolly.eventwarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.ArgumentFormat;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.main.Banlist;
import de.jolly.eventwarp.players.EventPlayer;

public class BanCommand extends AdvancedCommand {

	private Eventwarp warp;
	private Banlist banlist;
	
	public BanCommand(Eventwarp warp, Banlist banlist, CommandsHub hub) {
		super(hub);
		
		this.warp = warp;
		this.banlist = banlist;
		
		callArgument = "ban";
		argumentDescription = "<name>";
		description = "Bannt einen Spieler vom Eventwarp.";
		permission = "ban";
		isAdvanced = true;
		consoleAllowed = true;
		
		argFormat = new ArgumentFormat[] { ArgumentFormat.PLAYER };
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player target = Bukkit.getPlayerExact(args[0]);
		
		EventPlayer p = warp.getPlayerManager().getPlayer(target);
		if (p == null) {
			if (banlist.isBanned(target)) {
				sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich nicht am Eventwarp und ist bereits gebannt.");
			} else {
				banlist.banPlayer(target);
				sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM befindet sich nicht am Eventwarp, aber wurde soeben gebannt.");
			}
			return;
		}
		
		p.ban();
		sendMessage(sender, "Der Spieler �defS"+target.getName()+"�defM wurde vom Eventwarp gebannt.");
		sendMessage(target, "Du wurdest vom Eventwarp gebannt.");
	}

}
