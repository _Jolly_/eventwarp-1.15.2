package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import codetools.main.CodeTools;
import de.jolly.eventwarp.players.PlayerManager;

public class PlayerlistCommand extends AdvancedCommand {
	
	private PlayerManager manager;

	public PlayerlistCommand(PlayerManager manager, CommandsHub hub) {
		super(hub);

		this.manager = manager;

		callArgument = "players";
		argumentDescription = "";
		description = "Zeigt eine Liste aller aktuellen Spieler.";
		permission = "playerlist";
		isAdvanced = false;
		consoleAllowed = true;
	}
	
	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		sendMessage(sender, "�aAktuelle Spieler:\n�defM"+
		CodeTools.toString(manager.getAllPlayers()));
	}

}
