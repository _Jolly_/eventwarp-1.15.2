package de.jolly.eventwarp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import codetools.commands.AdvancedCommand;
import codetools.commands.CommandsHub;
import de.jolly.eventwarp.controls.Eventwarp;
import de.jolly.eventwarp.players.EventPlayer;
import de.jolly.eventwarp.players.PlayerManager;
import de.jolly.eventwarp.settings.tangible.SuicideSetting;

public class SuicideCommand extends AdvancedCommand {

	private final PlayerManager manager;
	private final SuicideSetting setting;
	
	public SuicideCommand(Eventwarp warp, CommandsHub hub) {
		super(hub);
		
		manager = warp.getPlayerManager();
		setting = (SuicideSetting) warp.getSettings().getSetting(SuicideSetting.NAME);
		
		callArgument = "suicide";
		argumentDescription = "";
		description = "Begeht Selbstmord.";
		permission = "";
		isAdvanced = false;
		consoleAllowed =  false;
	}

	@Override
	public void onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		EventPlayer player = manager.getPlayer(p);
		if (!setting.isEnabled()) {
			sendMessage(sender, "Selbstmord ist deaktiviert.");
			return;
		}
		if (player == null) {
			sendMessage(sender, "Das geht nur, w�hrend du im Event bist!");
			return;
		}
		if (player.isSpectatormode()) {
			sendMessage(sender, "Das geht im Spectatormode nicht!");
			return;
		}
		
		p.setHealth(0);
	}

}
