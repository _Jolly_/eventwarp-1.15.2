package de.jolly.eventwarp.region;

public class AreaDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9069549197326887517L;

	AreaDataException() {
		super("Could not successfully read area data!");
	}
	
	AreaDataException(Throwable cause) {
		super("Could not successfully read area data!", cause);
	}
	
}
