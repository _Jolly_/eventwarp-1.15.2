package de.jolly.eventwarp.region;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.BrewingStand;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Furnace;
import org.bukkit.block.data.Directional;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;

import codetools.main.CodeTools;

class BlockData {

	static final BlockFace[] all = BlockFace.values();
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + face;
		result = prime * result + type.ordinal();
		result = prime * result + inventoryId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockData other = (BlockData) obj;
		if (face != other.face)
			return false;
		if (type != other.type)
			return false;
		if (inventoryId != other.inventoryId)
			return false;
		return true;
	}

	private static byte retrieveFacing(Block b) {
		if (b.getBlockData() instanceof Directional) {
			BlockFace facing = ((Directional)b.getBlockData()).getFacing();
			for (byte i = 0; i < all.length; i++) {
				if (all[i] == facing) {
					return i;
				}
			}
		}
		return -1;
	}
	
	private Material type;
	private byte face;
	private short inventoryId;
	
	BlockData(Block b, short inventoryId) {
		type = b.getType();
		face = retrieveFacing(b);
		this.inventoryId = inventoryId;
	}
	BlockData(Material type, byte face, short inventoryId) {
		this.type = type;
		this.face = face;
		this.inventoryId = inventoryId;
	}
	
	void apply(Block b, ConfigurationSection inventories) {
		b.setType(type);
		if (face != -1) {
			Directional data = (Directional) b.getBlockData();
			data.setFacing(all[face]);
			b.setBlockData(data);
		}
		if (inventoryId != -1) {
			BlockState state = b.getState();
			Inventory inv = null;
			if (state instanceof Chest) {
				inv = ((Chest)state).getBlockInventory();
			} else if (state instanceof BrewingStand) {
				inv = ((BrewingStand)state).getInventory();
			} else if (state instanceof Dispenser) {
				inv = ((Dispenser)state).getInventory();
			} else if (state instanceof Furnace) {
				inv = ((Furnace)state).getInventory();
			} else {
				System.err.println("Could not successfully apply inventory contents! ("+b.getX()+"|"+b.getY()+"|"+b.getZ()+")");
				return;
			}
			inv.setContents(CodeTools.loadItemStackArray(inventories, String.valueOf(inventoryId)));
		}
	}
	
	Material getType() {
		return type;
	}
	int getFace() {
		return face;
	}
	
	short getInventoryId() {
		return inventoryId;
	}
	
	boolean equals(Block b) {
		return (type == b.getType()) && (face == retrieveFacing(b));
	}
	
	@Override
	public String toString() {
		return ("BlockData: Type="+getType()+" Face="+getFace()+" InvId="+getInventoryId());
	}
}
