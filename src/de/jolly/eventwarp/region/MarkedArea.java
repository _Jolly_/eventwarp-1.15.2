package de.jolly.eventwarp.region;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.BrewingStand;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Furnace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;

import codetools.main.CodeTools;

/**
 * This represents an area which was marked to do something with it.
 * @author _Jolly_
 * 
 */
public class MarkedArea {

	public static final String FILE_ENDING = "evarea";

	private World world;
	private int x1;
	private int y1;
	private int z1;
	private int x2;
	private int y2;
	private int z2;
	
	private boolean valid;
	private boolean pos1set;
	private boolean pos2set;
	
	public MarkedArea() {
		pos1set = false;
		pos2set = false;
		valid = false;
	}
	
	public MarkedArea(World world, int x1, int y1, int z1, int x2, int y2, int z2) {
		this.world = world;
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		this.z1 = z1;
		this.z2 = z2;
		pos1set = true;
		pos2set = true;
		validate();
	}

	public void setLoc1(World w, int x, int y, int z) {
		x1 = x;
		y1 = y;
		z1 = z;
		pos1set = true;
		if (world != w) {
			pos2set = false;
		}
		if (world != null && pos2set) {
			validate();
		} else {
			world = w;
			valid = false;
		}
	}
	public void setLoc2(World w, int x, int y, int z) {
		x2 = x;
		y2 = y;
		z2 = z;
		pos2set = true;
		if (world != w) {
			pos1set = false;
		}
		if (world != null && pos1set) {
			validate();
		} else {
			world = w;
			valid = false;
		}
	}
	private void validate() {
		valid = true;
		int oldx1 = x1;
		int oldy1 = y1;
		int oldz1 = z1;
		x1 = Math.min(x1, x2);
		y1 = Math.min(y1, y2);
		z1 = Math.min(z1, z2);
		x2 = Math.max(oldx1, x2);
		y2 = Math.max(oldy1, y2);
		z2 = Math.max(oldz1, z2);
	}
	/**
	 * Returns whether or not the marked area is valid, meaning both corners are set and in the same world.
	 */
	public boolean isValid() {
		return valid;
	}
	
	
	/**
	 * Saves the marked area into 2 different files with the same name but different endings.
	 * @param filename The names (paths) of the files. Endings are appended automatically.
	 */
	public void saveToFiles(String filename) {
		try {
			File file = new File(filename+"."+FILE_ENDING);
			file.createNewFile();
			File ymlfile = new File(filename+".yml");
			ymlfile.createNewFile();
			FileConfiguration config = YamlConfiguration.loadConfiguration(ymlfile);
			saveWorldDataToFiles(file, config);
			config.save(ymlfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveWorldDataToFiles(File worlddata, ConfigurationSection blockinventories) throws IOException {
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(worlddata));
		String wname = world.getName();
		dos.writeUTF(wname);
		
		dos.writeInt(x1);
		dos.writeInt(y1);
		dos.writeInt(z1);
		
		dos.writeInt(x2);
		dos.writeInt(y2);
		dos.writeInt(z2);
		
		short inventoryCounter = 0;
		//For each Block an ID is set which points to a specific BlockData
		ArrayList<Integer> ids = new ArrayList<>();
		//Every different BlockData gets its own ID, the specific blocks point to those IDs
		ArrayList<BlockData> dataList = new ArrayList<>();
		
		for (int x = x1; x <= x2; x++) {
			for (int y = y1; y <= y2; y++) {
				for (int z = z1; z <= z2; z++) {
					short inventoryId = -1;
					Block block = world.getBlockAt(x, y, z);
					if (attemptSaveInventory(block, blockinventories, inventoryCounter)) {
						inventoryId = inventoryCounter;
						inventoryCounter++;
					}
					BlockData data = new BlockData(block, inventoryId);
					int id = dataList.indexOf(data);
					if (id == -1) {
						id = dataList.size();
						dataList.add(data);
					}
					ids.add(id);
				}
			}
		}

		dos.writeInt(dataList.size());
		for (BlockData data: dataList) {
			dos.writeUTF(data.getType().name());
			dos.writeByte(data.getFace());
			dos.writeShort(data.getInventoryId());
		}
		
		dos.writeInt(ids.size());
		for (int id: ids) {
			dos.writeInt(id);
		}
		dos.close();
	}
	private boolean attemptSaveInventory(Block b, ConfigurationSection config, int id) {
		BlockState state = b.getState();
		Inventory inv = null;
		if (state instanceof Chest) {
			inv = ((Chest)state).getBlockInventory();
		} else if (state instanceof BrewingStand) {
			inv = ((BrewingStand)state).getInventory();
		} else if (state instanceof Dispenser) {
			inv = ((Dispenser)state).getInventory();
		} else if (state instanceof Furnace) {
			inv = ((Furnace)state).getInventory();
		} else return false;
		
		CodeTools.saveItemStackArray(inv.getContents(), config, String.valueOf(id));
		
		return true;
	}

	/**
	 * Rebuilds an area which has been saved into files.
	 * @param filename The names (paths) of the files which store the world data.
	 * @throws AreaDataException If the given files are corrupted.
	 */
	public static void rebuildFromFiles(String filename) throws AreaDataException {
		try {
			File worlddata = new File(filename+"."+FILE_ENDING);
			File inventorydata = new File(filename+".yml");
			FileConfiguration config = YamlConfiguration.loadConfiguration(inventorydata);
			rebuildFromWorldData(worlddata, config);
		} catch (Exception e) {
			throw new AreaDataException(e);
		}
	}

	private static void rebuildFromWorldData(File worlddata, ConfigurationSection blockinventories) throws IOException {
		DataInputStream dis = new DataInputStream(new FileInputStream(worlddata));
		String wname = dis.readUTF();
		World world = Bukkit.getWorld(wname);
		int x1 = dis.readInt();
		int y1 = dis.readInt();
		int z1 = dis.readInt();

		int x2 = dis.readInt();
		int y2 = dis.readInt();
		int z2 = dis.readInt();
		
		int length = dis.readInt();
		assert (length >= 0);
		ArrayList<BlockData> list = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			list.add(new BlockData(
					Material.getMaterial(dis.readUTF()),
					dis.readByte(),
					dis.readShort()));
		}
		
		int blocks = dis.readInt();
		assert (blocks == (x2-x1+1)*(y2-y1+1)*(z2-z1+1));
		
		for (int x = x1; x <= x2; x++) {
			for (int y = y1; y <= y2; y++) {
				for (int z = z1; z <= z2; z++) {
					BlockData data = list.get(dis.readInt());
					data.apply(world.getBlockAt(x, y, z), blockinventories);
				}
			}
		}
		dis.close();
	}
}
