package de.jolly.eventwarp.main;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class Banlist {

	private final String filename = "banlist.yml";

	/**
	 * Die Liste mit allen gebannten UUIDs (jeweils als String)
	 */
	private Set<String> bannedIds;
	/**
	 * Die Referenz zur Main
	 */
	private Plugin main;

	public Banlist(Plugin main) {
		this.main = main;
		bannedIds = new HashSet<>();
		loadBanlist();
	}

	/**
	 * Gibt zur�ck, ob der Spieler in der Banliste steht
	 * @param p Der Spieler
	 * @return true, wenn die UUID dieses Spielers in der Banliste steht, sonst false
	 */
	public boolean isBanned(Player p) {
		return bannedIds.contains(p.getUniqueId().toString());
	}
	/**
	 * F�gt einen Spieler zur Banlist hinzu
	 * @param p Der Spieler, der auf die Banliste geschrieben werden soll
	 */
	public void banPlayer(Player p) {
		bannedIds.add(p.getUniqueId().toString());
		saveBanlist();
	}
	/**
	 * Entfernt einen Spieler von der Banliste
	 * @param p Der Spieler, der von der Banliste entfernt werden soll.
	 */
	public void unbanPlayer(Player p) {
		bannedIds.remove(p.getUniqueId().toString());
		saveBanlist();
	}
	/**
	 * Entfernt alle Eintr�ge aus der Banliste
	 */
	public void clear() {
		bannedIds.clear();
		saveBanlist();
	}

	/**
	 * Speichert die Banliste in eine banlist.yml-Datei
	 */
	public void saveBanlist() {
		try {

			File file = new File(main.getDataFolder() + File.separator + filename);
			if (!file.exists()) file.createNewFile();
			FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
			cfg.set("size", bannedIds.size());
			int i = 0;
			for (String id: bannedIds) {
				cfg.set("list."+i, id);
				i++;
			}
			cfg.save(file);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * L�dt die Banliste aus der Datei, falls es eine gibt
	 */
	public void loadBanlist() {
		File file = new File(main.getDataFolder() + File.separator + filename);
		if (!file.exists()) return;
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		int size = cfg.getInt("size");
		for (int i = 0; i < size; i++) {
			if (cfg.getString("list."+i) == null) continue;
			bannedIds.add(cfg.getString("list."+i));
		}
	}

}