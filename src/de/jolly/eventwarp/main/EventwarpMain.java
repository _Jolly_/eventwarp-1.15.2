package de.jolly.eventwarp.main;

import codetools.commands.CommandsHub;
import codetools.main.AdvancedJavaPlugin;
import de.jolly.eventwarp.commands.*;
import de.jolly.eventwarp.controls.Eventwarp;

public class EventwarpMain extends AdvancedJavaPlugin {

	private Banlist banlist;
	private Eventwarp warp;

	private CommandsHub eventCommandHub;

	private final String eventCommand = "event";

	@Override
	public void onEnable() {
		try {
			super.onEnable();

			command = "eventwarp";
			permission = "eventwarp";

			banlist = new Banlist(this);
			warp = new Eventwarp(this);

			eventCommandHub = new CommandsHub(this, getErrorHandler());

			getCommand(command).setExecutor(getCommandsHub());
			getCommand(eventCommand).setExecutor(eventCommandHub);
			eventCommandHub.setCommandString(eventCommand);

			createCommands();
			getCommand(command).setTabCompleter(getCommandsHub().createTabCompleter());
			getCommand(eventCommand).setTabCompleter(eventCommandHub.createTabCompleter());

			sendMessage(getServer().getConsoleSender(), "�bDer Eventwarp braucht fuer sein Ego 'ne farbige Nachricht, wenn er erfolgreich gestartet wurde...");
		} catch (Exception e) {
			getErrorHandler().EnableExceptionOccured(e);
		}
	}

	private void createCommands() {

		getCommandsHub().registerSubCommand(new OpenCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new CloseCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new KickCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new KickallCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new ControlPanelCommand(warp.getSettings().getControlPanel(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new StatusCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new PlayerlistCommand(warp.getPlayerManager(), getCommandsHub()));

		getCommandsHub().registerSubCommand(new LogCommand(warp.getLog(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new AttachCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new DetachCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new BanCommand(warp, banlist, getCommandsHub()));
		getCommandsHub().registerSubCommand(new UnbanCommand(banlist, getCommandsHub()));
		getCommandsHub().registerSubCommand(new UnbanallCommand(banlist, getCommandsHub()));
		getCommandsHub().registerSubCommand(new AddCommand(warp, this, getCommandsHub()));
		getCommandsHub().registerSubCommand(new KitCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new ArmorCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new SpawnLocationCommand(warp.getSpawnManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new SetLivesCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new ResetCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new ResetAllCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new SetTeamCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new AssignTeamsCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new ClearTeamsCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new SetSpawngroupCommand(warp.getPlayerManager(), getCommandsHub()));
		getCommandsHub().registerSubCommand(new SpawngroupInfoCommand(getCommandsHub()));
		getCommandsHub().registerSubCommand(new EnableLobbyModeCommand(warp, getCommandsHub()));
		getCommandsHub().registerSubCommand(new EnableGameModeCommand(warp, getCommandsHub()));

		getCommandsHub().registerSubCommand(new AreaMarkerCommand(this, getCommandsHub()));
		getCommandsHub().registerSubCommand(new AreaSaveCommand(this, getCommandsHub()));
		getCommandsHub().registerSubCommand(new AreaLoadCommand(this, getCommandsHub()));


		eventCommandHub.registerMainCommand(new EventCommand(this, warp, banlist, eventCommandHub));
		eventCommandHub.registerSubCommand(new LeaveCommand(warp, eventCommandHub));
		eventCommandHub.registerSubCommand(new SuicideCommand(warp, eventCommandHub));
	}

	public Banlist getBanlist() {
		return banlist;
	}

	public String getEventCommand() {
		return eventCommand;
	}

}